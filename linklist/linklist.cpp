#include <stdio.h>
#include <stdbool.h>
#include <malloc.h>
#define scanf scanf_s
#define Elemtype int

typedef struct Lnode {
	Elemtype data;
	struct Lnode* next;
}Lnode, * Linklist;
//typedef struct LNode LNode; //将结构体重命名为LNode
//typedef struct LNode* LinkList; //将结构体指针重命名为LinkList
//LNode是一个具象的结构体类型，指向的是包含某个数据类型的数据域和指针域的结构体类型。
//而LinkList是LNode的指针类型，它占用一定内存空间，内存空间中的值是一个LNode类型结构体的地址。相比LNode，它显得有些“虚无缥缈”。

typedef struct Dnode {
	Elemtype data;
	struct Dnode* prior, * next;
}Dnode, *DLinklist;//双链表

#define Maxsize 50
typedef struct {
	Elemtype data;
	int next;
}SLinklist[Maxsize];//静态链表

Linklist list_HeadInsert(Linklist& L) {
	Lnode* s;
	int x;
	L = (Linklist)malloc(sizeof(Lnode));//创建头结点
	L->next = NULL;//初始为空链表
	scanf("%d", &x);//输入结点的值
	while (x != 9999) {//结束标志，结束输入
		s = (Lnode*)malloc(sizeof(Lnode));//创建新结点
		s->next = L->next;
		L->next = s;//将新结点插入表中，L为头指针
		scanf("%d", &x);
	}
	return L;
}//头插法建立单链表

Linklist List_TailInsert(Linklist& L) {
	int x;
	Lnode* s, * r = L;//r为表尾指针
	L = (Linklist)malloc(sizeof(Lnode));
	L->next = NULL;
	scanf("%d", &x);
	while (x != 9999) {
		s = (Lnode*)malloc(sizeof(Lnode));
		r->next = s;
		s->data = x;
		r = s;
		scanf("%d", &x);
	}
	r->next = NULL;//尾结点置空
	return L;
}//尾插法建立单链表

Lnode* GetElem(Linklist L, int i) {
	int j = 1;
	Lnode* p = L->next;
	if (i == 0)
		return L;
	if (i < 1)
		return NULL;
	while (p && j < i) {
		p = p->next;
		j++;
	}
	return p;
}//按序号查找结点值

Lnode* LocateElem(Linklist L, Elemtype e) {
	Lnode* p = L->next;
	while (p && p->data != e)
		p = p->next;
	return p;
}//按值查找结点

void LinklistInsert(Linklist& L, int i, Lnode*& s) {
	Lnode* p = GetElem(L, i - 1);
	s->next = p->next;
	p->next = s;
}//前插法插入结点

void LinklistTailInsert(Linklist& L, int i, Lnode*& s) {
	Elemtype temp;
	Lnode* p = GetElem(L, i - 1);
	s->next = p->next;
	p->next = s;
	temp = s->data;
	s->data = p->data;
	p->data = temp;
}//后插法插入结点

void DelLinklist(Linklist& L, int i) {
	Lnode* p = GetElem(L, i - 1);
	Lnode* q = p->next;
	p->next = q->next;
	free(q);
}//删除位置i的结点

void DelLinklist_p(Linklist& L, Lnode* p) {
	Lnode* q = p->next;
	p->data = p->next->data;
	p->next = q->next;
	free(q);
}//删除p结点

void R_Print(Linklist L) {//利用递归从尾到头输出单链表L中每个结点的值
	if (L->next != NULL) {//L的后继非空
		R_Print(L->next);//进入递归
	}
	if (L != NULL) printf("%d", L->data);//从递归返回,L非空则打印输出其值
}

void Rverse(Linklist& L) {//将L就地逆置
	Lnode* p, * q, * k;
	p = L->next;//前置指针
	q = p->next;//修改指针
	p->next = NULL;//第一个结点放置最后
	k = q->next;
	while (q != NULL) {
		q->next = p;
		p = q;
		q = k;
		k = p->next;
	}
	L->next = q;
}
//page39页,22题,时间尽可能高效,找到共同后缀的起始位置
//1,分别求出str1与str2所指的两个链表的长度m和n;
// 2,将两个链表以表尾对齐,令指针p与q分别指向str1和str2的头结点,若m>=n,指针p先走,使p指向链表中的第m-n+1个结点;若m<n,
//则使q指向链表中的n-m+1个结点,即使p和q所指的结点到表尾的长度相等;
//3,反复将指针p和q同步向后移动,当p和q指向同一位置时停止,即为共同后缀的起始位置,算法结束.
typedef struct Linklist_22
{
	char data;
	struct Linklist_22* next;
}SNode;
//求链表长度的函数
int listlen(SNode* head)
{
	int len = 0;
	while (head->next != NULL)//下个结点非空,长度加一,循环到最后一个结点
	{
		len++;
		head = head->next;
	}
	return len;
}
SNode* find_addr(SNode* str1, SNode* str2)
{
	int m, n;
	SNode* p, * q;
	m = listlen(str1);//求str1的长度
	n = listlen(str2);//求str2的长度
	for (p = str1; m > n; m--)//若m>n,使p指向第m-n+1个结点
		p = p->next;
	for (q = str2; n > m; n--)//若n>m,使q指向第n-m+1个结点
		q = q->next;
	while (p->next != NULL && p->next != q->next)//指针p和q同步后移
	{
		p = p->next;
		q = q->next;
	}
	return p->next;//返回共同后缀的起始地址
}
//时间复杂度为O(len1+len2)或O(max(len1,len2)),其中len1,len2分别为两个链表的长度. 


//page39页,23题,删除绝对值相同的点,仅保留出现的第一个
//算法的核心思想是用空间换时间.使用辅助数组记录链表中已出现的数值,从而只需对链表进行一趟扫描
//因为|data|<=n,故辅助数组的大小为n+1,各元素的初始值均为0.依次扫描链表中的各结点,同时检查q[|data|]的值,若为0则保留该结点,并令q[|data|]= 1;
//否则将该结点从链表中删除
//时间复杂度为O(m),空间复杂度为O(n)
typedef struct Linklist_23
{
	int data;//数据域
	struct Linklist_23* link;//指针域
}*PNODE;
void Del_sametrue(PNODE S, int n)
{
	PNODE p = S, r;
	int m, * q;//
	q = (int*)malloc(sizeof(int) * (n + 1));//申请n+1个辅助空间
	for (int i = 0; i < n + 1; i++)//数组元素值置0
		*(q + 1) = 0;
	while (p->link != NULL)
	{
		m = p->link->data > 0 ? p->link->data : -p->link->data;//取绝对值
		if (*(q + m) == 0)//判断该结点的值是否出现过
		{
			*(q + m) = 1;//未出现则保留
			p = p->link;
		}
		else
		{
			r = p->link;//已出现则删除
			p->link = r->link;
			free(r);
		}
	}
	free(q);//释放q所申请的空间
}