#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
//结构体
//struct stu
//{
//	char name[20];//姓名
//	int class;//班级
//	float grade;//成绩
//}S1,S2;
////S1,S2为struct stu类型的全局变量
struct
{
	char name[20];//姓名
	int class;//班级
	float grade;//成绩
}S3, name;
typedef struct stu
{
	char name[20];//姓名
	int class;//班级
	float grade;//成绩
}Stu;
//Stu为typedef将struct stu类型改名为Stu
int main()
{
	struct stu zhangsan = { {"张三"}, 1, 80.0};
	struct stu* p = &zhangsan;
	printf("%s ", zhangsan.name);
	printf("%d ", zhangsan.class);
	printf("%0.2f\n", zhangsan.grade);
	printf("%s ", p->name);
	printf("%d ", p->class);
	printf("%0.2f\n", p->grade);
	return 0;
}
//Stu lisi = { {"李四"}, 2, 90.0 };
	/*printf("\n%s ", lisi.name);
	printf("%d ", lisi.class);
	printf("%0.2f ", lisi.grade);*/
Stu S1 = { {"测试1"}, 1, 20.0 }, S2;