#define _CRT_SECURE_NO_WARNINGS 1

#include"Seqlist.h"

// 初始化
void SeqListInit(SeqList* ps)
{
	ps->data = (SLDateType*)malloc(sizeof(SLDateType) * 4);
	if (ps->data == NULL)
	{
		perror("malloc::");
		exit(-1);
	}
	ps->size = 0;
	ps->capacity = 4;
}

// 销毁
void SeqListDestroy(SeqList* ps)
{
	free(ps->data);
	ps->data = NULL;
	ps->capacity = ps->size = 0;
}

// 打印
void SeqListPrint(SeqList* ps)
{
	assert(ps);
	assert(ps->size);
	int i = 0;
	for (i = 0; i < ps->size; i++)
	{
		printf("%d ", ps->data[i]);
	}
	printf("\n");
}

// 检查容量
void CheckCapacity(SeqList* ps)
{
	assert(ps);
	// 容量已满则扩容
	if (ps->size == ps->capacity)
	{
		ps->data = (SLDateType*)realloc(ps->data, sizeof(SLDateType) * (ps->size + 3));
		ps->capacity += 3;
	}
}

// 尾插数据
void SeqListPushBack(SeqList* ps, SLDateType x)
{
	CheckCapacity(&ps);
	// 插入数据
	ps->data[ps->size] = x;
	ps->size++;
}

// 头插数据
void SeqListPushFront(SeqList* ps, SLDateType x)
{
	CheckCapacity(&ps);
	// 数据后移
	int i = 0;
	for (i = 0; i < ps->size; i++)
	{
		ps->data[ps->size - i] = ps->data[ps->size - i - 1];
	}
	ps->data[0] = x;
	ps->size++;
}

// 头删数据
void SeqListPopFront(SeqList* ps)
{
	assert(ps);
	assert(ps->size);
	// 数据前移
	int i = 0;
	for (i = 0; i < ps->size - 1; i++)
	{
		ps->data[i] = ps->data[i + 1];
	}
	ps->size--;
}

// 尾删数据
void SeqListPopBack(SeqList* ps)
{
	assert(ps);
	assert(ps->size);
	ps->size--;
}

// 顺序表查找
int SeqListFind(SeqList* ps, SLDateType x)
{
	assert(ps);
	assert(ps->size);
	int i = 0;
	for (i = 0; i < ps->size; i++)
	{
		if (ps->data[i] == x)
			return i + 1;
	}
	return -1;
}

// 顺序表在pos位置插入x
void SeqListInsert(SeqList* ps, int pos, SLDateType x)
{
	assert(ps);
	if (pos < 1 || pos > ps->size + 1) // 保证在pos位置
	{
		perror("pos::");
		exit(-1);
	}
	CheckCapacity(&ps);
	// 数据后移
	int i = 0;
	for (i = 0; i < ps->size - pos + 1; i++) // 后续size - pos个元素需要移动，原本在pos位置的元素也需要移动，所以+1
	{
		ps->data[ps->size - i] = ps->data[ps->size - i - 1];
	}
	ps->data[pos - 1] = x;
	ps->size++;
}

// 顺序表删除pos位置的值
void SeqListErase(SeqList* ps, int pos)
{
	assert(ps);
	assert(ps->size);
	if (pos < 1 || pos > ps->size)
	{
		perror("pos::");
		exit(-1);
	}
	// 数据前移
	int i = 0;
	for (i = 0; i < ps->size - pos; i++) // 后续size - pos个元素需要前移
	{
		ps->data[pos + i - 1] = ps->data[pos + i];
	}
	ps->size--;
}