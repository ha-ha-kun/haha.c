#pragma once
#include<stdio.h>
#include<string.h>
#include<assert.h>
#include<malloc.h>

//联系人结构体
typedef struct pep
{
	//姓名
	char name[20];
	//性别
	char sex[3];
	//年龄
	int age;
	//电话
	char tel[15];
	//地址
	char addr[30];
	//指向下一个结构体
	struct pep* next;
}pep;

//通讯录结构体
typedef struct Contact
{
	pep data;
	int sz;
}Contact;

//枚举提高代码可读性
enum
{
	EXIT,
	ADD,
	DEL,
	SEARCH,
	MODIFY,

};

//初始化
void InitContact(Contact* con);
//添加联系人
void Add(Contact* con);
//以名字搜寻联系人
Contact* SearchByName(Contact* con, char* n);
//打印指定联系人
void PrintP(pep* p);
//搜索联系人
void Search(Contact* con);