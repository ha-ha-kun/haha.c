#define _CRT_SECURE_NO_WARNINGS 1
#include"contact.h"

void menu()
{
	printf("**************************** *******\n");
	printf("******   1、add     2、del    ******\n");
	printf("******   3、search  4、modify ******\n");
	printf("******   0、exit              ******\n");
	printf("************************************\n");
 	printf("************************************\n");
}

int main()
{
	int input = 0;
	Contact  con;
	InitContact(&con);
	do
	{
		menu();
		scanf("%d", &input);
		switch (input)
		{
		case ADD:
			Add(&con);
			break;
		case DEL:
			break;
		case SEARCH:
			Search(&con);
			break;
		case MODIFY:
			break;
		case EXIT:
			printf("退出通讯录");
			break;
		default:
			printf("输入错误，请重新输入：");
			break;
		}
	} while (input);
}