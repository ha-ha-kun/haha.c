#define _CRT_SECURE_NO_WARNINGS 1
#include"contact.h"

void InitContact(Contact* con)
{
	assert(con);
	//头结点为空
	con = (Contact*)malloc(sizeof(Contact));
	con->data.next = NULL;
	con->sz = 0;
}
void Add(Contact* con)
{
	assert(con);
	//申请空间
	pep* p = (pep*)malloc(sizeof(pep));
	//头插法插入结点
	p->next = con->data.next;
	con->data.next = p;
	//修改信息
	printf("请输入姓名：");
	scanf("%s", p->name);
	printf("请输入性别：");
	scanf("%s", p->sex);
	printf("请输入年龄：");
	scanf("%d", &(p->age));
	printf("请输入电话：");
	scanf("%s", p->tel);
	printf("请输入地址：");
	scanf("%s", p->addr);
	con->sz++;
}


//用名字搜索联系人
Contact* SearchByName(Contact* con, char* n)
{
	//跳过头结点
	pep* ptr = NULL;
	ptr = con->data.next;
	while (strcmp(ptr->name, n) != 0 && ptr != NULL)
	{
		//寻找下一个结点
		ptr = ptr->next;
	}
	if (ptr == NULL)
	{
		//没找到，返回NULL
		return NULL;
	}
	else
	{
		return ptr;
	}
}

//打印指定联系人
void PrintP(pep* p)
{
	if (p != NULL)
	{
		printf("%20s %5s %5s %15s %30s\n", "姓名", "性别", "年龄", "电话", "住址");
		printf("%20s %5s %5d %15s %30s\n", p->name, p->sex, p->age, p->tel, p->addr);

	}
	else
	{
		printf("不存在此联系人");
	}
}

//打印联系表
void PrintC(Contact* con)
{
	int i = 0;
	pep* p = con->data.next;
	printf("%5s %20s %5s %5s %15s %30s\n", "序号", "姓名", "性别", "年龄", "电话", "住址");

	for (i = 0; i < con->sz; i++)
	{
		//打印
		printf("%5d %20s %5s %5d %15s %30s\n", i + 1, p->name, p->sex, p->age, p->tel, p->addr);
		//后移
		p = p->next;
	}
}

//搜索
void Search(Contact* con)
{
	assert(con);
	char* n[20] = { 0 };
	printf("请输入你要搜索的人：");
	scanf("%s", n);
	PrintP(SearchByName(con, n));
}
