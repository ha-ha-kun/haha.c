#define _CRT_SECURE_NO_WARNINGS 1
#include"SL.h"

// 申请新节点
SListNode* BuyNewNode(SLDataType x)
{
	SListNode* tmp = (SListNode*)malloc(sizeof(SListNode));
	assert(tmp);
	tmp->data = x;
	tmp->next = NULL;
	return tmp;
}

// 打印
void SLPrint(SList phead)
{
	SListNode* cur = phead;
	while (cur)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}

// 尾插
void SLPushBack(SList* pphead, SLDataType x) // 需要对链表修改，故传指向链表的指针，由于链表是指向结点的指针，故 SList* 换为 SListNode** 也可
{
	assert(pphead);
	SListNode* newnode = BuyNewNode(x);
	if (*pphead == NULL)
	{
		*pphead = newnode;
	}
	else
	{
		// 找尾结点
		SListNode* tail = *pphead;
		while (tail->next)
		{
			tail = tail ->next;
		}
		tail->next = newnode;
	}
}

void SLPushFront(SList* pphead, SLDataType x)
{
	assert(pphead);
	SListNode* newnode = BuyNewNode(x);
	assert(newnode);
	if ((*pphead) == NULL)
	{
		*pphead = newnode;
	}
	else
	{
		newnode->next = *pphead;
		*pphead= newnode;
	}	
}

void SLPopBack(SList* pphead)
{
	assert(pphead);
	assert(*pphead);
	if ((*pphead)->next == NULL)// 只有一个结点
	{
		free(*pphead);
		(*pphead) = NULL;
	}
	else
	{
		SListNode* pre = *pphead;
		SListNode* tail = (*pphead)->next;
		while (tail->next)
		{
			pre = tail;
			tail = tail->next;
		}
		free(tail);
		pre->next = NULL;
	}
}

void SLPopFront(SList* pphead)
{
	assert(pphead);
	assert(*pphead);
	SListNode* head = *pphead;
	*pphead = (*pphead)->next;
	free(head);
}

SListNode* SLFind(SList phead, SLDataType x)
{
	assert(phead);
	SListNode* cur = phead;
	while (cur && cur->data != x)
	{
		cur = cur->next;
	}
	return cur;
}

void SListInsert(SList* pphead, SListNode* pos, SLDataType x)
{
	assert(pphead);
	assert(*pphead);
	assert(pos);
	SListNode* cur = *pphead;
	// 寻找pos之前的结点
	if (cur == pos)
	{
		SLPushFront(pphead, x);
		return;
	}
	while (cur && cur->next != pos)
	{
		cur = cur->next;
	}
	// 原则上来说，使用SLFind查找的结点，不会运行以下if语句，pos只可能是链表中的结点，或为空，而为空时，assert语句会报错
	if (cur == NULL)
	{
		perror("SListInsert::pos::");
		exit(1);
	}
	SListNode* newnode = BuyNewNode(x);
	newnode->next = pos;
	cur->next = newnode;
}

void SListInsertAfter(SListNode* pos, SLDataType x)
{
	assert(pos);
	SListNode* newnode = BuyNewNode(x);
	assert(newnode);
	newnode->next = pos->next;
	pos->next = newnode;
}

void SListEraseAfer(SListNode* pos)
{
	assert(pos);
	assert(pos->next);
	pos->next = pos->next->next;
	free(pos->next);
}

void SListDestroy(SList* pphead)
{
	assert(pphead);
	assert(*pphead);
	SListNode* tmp = NULL;
	SListNode* cur = *pphead;
	while (cur)
	{
		tmp = cur;
		cur = cur->next;
		free(tmp);
	}
	*pphead = NULL;
}