#define _CRT_SECURE_NO_WARNINGS 1

#include"SL.h"

void test1()
{
	SList s = NULL;
	SLPrint(s);
	SLPushBack(&s, 1);
	SLPrint(s);
	SLPushBack(&s, 2);
	SLPrint(s);
}

void test2()
{
	SList s = NULL;
	SLPrint(s);
	SLPushFront(&s, 1);
	SLPrint(s);
	SLPushFront(&s, 2);
	SLPrint(s);
	SLPushFront(&s, 3);
	SLPrint(s);
	SLPushFront(&s, 4);
	SLPrint(s);
}

void test3()
{
	SList s = NULL;
	SLPrint(s);
	SLPushFront(&s, 1);
	SLPrint(s);
	SLPopBack(&s);
	SLPrint(s);
	SLPushFront(&s, 2);
	SLPrint(s);
	SLPushFront(&s, 3);
	SLPrint(s);
	SLPushFront(&s, 4);
	SLPrint(s);
	SLPopBack(&s);
	SLPrint(s);
}

void test4()
{
	SList s = NULL;
	SLPrint(s);
	// SLPopFront(&s);
	SLPushFront(&s, 1);
	SLPrint(s);
	SLPopFront(&s);
	SLPrint(s);
	SLPushFront(&s, 2);
	SLPrint(s);
	SLPushFront(&s, 3);
	SLPrint(s);
	SLPushFront(&s, 4);
	SLPrint(s);
	SLPopFront(&s);
	SLPrint(s);
}

void test5()
{
	SList s = NULL;
	SLPrint(s);
	// SLPopFront(&s);
	SLPushFront(&s, 1);
	SLPrint(s);
	SLPushFront(&s, 2);
	SLPrint(s);
	SLPushFront(&s, 3);
	SLPrint(s);
	SLPushFront(&s, 4);
	SLPrint(s);
	printf("%p\n", SLFind(s, 1));
	printf("%p\n", SLFind(s, 5));
}

void test6()
{
	SList s = NULL;
	SLPushFront(&s, 1);
	SLPushFront(&s, 2);
	SLPushFront(&s, 3);
	SLPushFront(&s, 4);
	SLPrint(s);
	SListInsert(&s, SLFind(s, 7), 5);
	SLPrint(s);
}

void test8()
{
	SList s = NULL;
	SLPushFront(&s, 1);
	SLPushFront(&s, 2);
	SLPushFront(&s, 3);
	SLPushFront(&s, 4);
	SLPushFront(&s, 5);
	SLPrint(s);
	SListEraseAfer(SLFind(s, 2));
	SLPrint(s);
}

void test7()
{
	SList s = NULL;
	SLPushFront(&s, 1);
	SLPushFront(&s, 2);
	SLPushFront(&s, 3);
	SLPushFront(&s, 4);
	SLPrint(s);
	SListInsertAfter(&s, SLFind(s, 4), 5);
	SLPrint(s);
}
int main()
{
	test8();
	return 0;
}