#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

typedef int SLDataType;
typedef struct SListNode
{
	SLDataType data;
	struct SListNode* next;
// SList为指向结构体指针，SListNode为结构体
}*SList, SListNode;

// 申请新节点
SListNode* BuyNewNode(SLDataType x);

// 打印
void SLPrint(SList s);

// 尾插
void SLPushBack(SList* pphead, SLDataType x);

// 头插
void SLPushFront(SList* pphead, SLDataType x);

// 尾删
void SLPopBack(SList* pphead);

// 头删
void SLPopFront(SList* pphead);

// 按值查找，返回找到的结点地址，未找到返回NULL
SListNode* SLFind(SList phead, SLDataType x);

// 在pos之前插入
void SListInsert(SList* pphead, SListNode* pos, SLDataType x);

// 在pos之后插入
void SListInsertAfter(SListNode* pos, SLDataType x);

// 删除pos之后的值
void SListEraseAfer(SListNode* pos);

// 单链表的销毁
void SListDestroy(SList* pphead);