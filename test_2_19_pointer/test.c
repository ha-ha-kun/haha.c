#define _CRT_SECURE_NO_WARNINGS 1

////写一个函数打印arr数组的内容，不使用数组下标，使用指针。
////arr是一个整形一维数组。
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* p = arr;
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	return 0;
//}

////写一个函数，可以逆序一个字符串的内容。
//char reverse(char* a)
//{
//	int sz = 0;
//	char tmp = 0;
//	//计算字符串大小
//	for (int i = 0; a[i] != '\0'; i++)
//	{
//		sz++;
//	}
//	//交换字符串内容
//	for (int i = 0; i <= sz / 2; i++)
//	{
//		tmp = a[i];
//		a[i] = a[sz - i - 1];
//		a[sz - i - 1] = tmp;
//		
//	}
//	return a;
//}
//#include<stdio.h>
//int main()
//{
//	char arr[] = { "abcdef" };
//	reverse(arr);
//	printf("%s", arr);
//	return 0;
//}

////求Sn=a+aa+aaa+aaaa+aaaaa的前5项之和，其中a是一个数字，例如：2 + 22 + 222 + 2222 + 22222
//#include<stdio.h>
//#include<math.h>
//int sumbit(int a)
//{
//	int x = 0;
//	int sum = 0;
//	for (int i = 0; i < 5; i++)
//	{
//		x += a * pow(10,i);
//		sum += x;
//	}
//	return sum;
//}
//int main()
//{
//	int x = 0;
//	scanf("%d", &x);
//	printf("%d",sumbit(x));
//	return 0;
//}
//
////求出0～100000之间的所有“水仙花数”并输出。
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//	for (int i = 0; i < 100000; i++)
//	{
//		if (i < 10)
//		{
//			if (pow(i, 1) == i)
//				printf("%d ", i);
//		}
//		else if (i < 100)
//		{
//			if (pow(i / 10, 2) + pow(i % 10, 2) == i)
//				printf("%d ", i);
//		}
//		else if (i < 1000)
//		{
//			if (pow(i / 100, 3) + pow((i % 100)/ 10, 3) + pow(i % 10, 3) == i)
//				printf("%d ", i);
//		}
//		else if (i < 10000)
//		{
//			if (pow(i / 1000, 4) + pow((i % 1000) / 100, 4) + pow((i % 100) / 10, 4) + pow(i % 10, 4) == i)
//				printf("%d ", i);
//		}
//		else
//		{
//			if (pow(i / 10000, 5) + pow((i % 10000) / 1000, 5) + pow((i % 1000) / 100, 5) + pow((i % 100) / 10, 5) + pow(i % 10, 5) == i)
//				printf("%d ", i);
//		}
//	}
//	return 0;
//}

//打印菱形
#include<stdio.h>
int main()
{
	char arr[] = { "      *      " };
	for (int i = 0; i < 7; i++)
	{
		for (int j = 0; j <= i; j++)
		{
			arr[6 + j] = '*';
			arr[6 - j] = '*';
		}
		printf("%s\n", arr);
	}
	for (int i = 0; i < 7; i++)
	{
		for (int j = 0; j <= i; j++)
		{
			arr[j] = ' ';
			arr[12 - j] = ' ';
		}
		printf("%s\n", arr);
	}
	return 0;
}