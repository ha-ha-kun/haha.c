#define _CRT_SECURE_NO_WARNINGS 1
#include"Slist.h"



SListNode* BuySListNode(SLTDateType x)
{
	SListNode* newnode = (SListNode*)malloc(sizeof(SListNode));
	if (newnode == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}

	newnode->data = x;
	newnode->next = NULL;
	
	return newnode;
}


void SListPrint(SListNode* plist)
{
	assert(plist);
	// 增加cur指针，提高代码可读性，同时不改变形参plist，良好的代码习惯
	SListNode* cur = plist;

	while (cur)
	{
		printf("%d ", cur->data);
		cur = cur->next;
	}

	printf("\n");
}

void SListPushBack(SListNode** pplist, SLTDateType x)
{
	// 使用二级指针，因为当链表初始为空时，需要修改原地址，使其指向新申请的结点
	// 申请新节点
	SListNode* new = BuySListNode(x);
	if (*pplist == NULL)
	{
		*pplist = new;
	}
	else
	{
		SListNode* tail = *pplist;
		// 找尾
		while (tail->next != NULL)
		{
			tail = tail->next;
		}

		tail->next = new;
	}
}

void SListPushFront(SListNode** pplist, SLTDateType x)
{
	//// 同尾插，头插更简单
	//SListNode* new = BuySListNode(x);
	//if (*pplist == NULL)
	//{
	//	*pplist = new;
	//}
	//else
	//{
	//	SListNode* next = *pplist;
	//	*pplist = new;
	//	(*pplist)->next = next;
	//}

	// 可更简单化处理
	SListNode* new = BuySListNode(x);

	new->next = *pplist;
	*pplist = new;
}

void SListPopBack(SListNode** pplist)
{
	assert(pplist);
	assert(*pplist);

	if ((*pplist)->next == NULL)
	{
		free(*pplist);
		*pplist = NULL;
	}
	else
	{
		// 找尾
		SListNode* cur = *pplist;
		SListNode* tail = cur->next;
		while (tail->next)
		{
			cur = tail;
			tail = tail->next;
		}
		free(tail);
		cur->next = NULL;
	}
}

void SListPopFront(SListNode** pplist)
{
	assert(pplist);
	assert(*pplist);

	SListNode* next = (*pplist)->next;
	free(*pplist);
	*pplist = next;
}

SListNode* SListFind(SListNode* plist, SLTDateType x)
{
	assert(plist);

	SListNode* cur = plist;
	// 以cur为空指针作为退出循环的条件，但此时，cur需要放置在前，否则cur->data即是访问空指针。
	while (cur && cur->data != x)
	{
		cur = cur->next;
	}
	
	return cur ? cur : NULL;
}

void SListInsertAfter(SListNode* pos, SLTDateType x)
{
	assert(pos);

	SListNode* new = BuySListNode(x);
	new->next = pos->next;
	pos->next = new;
}

void SLTInsertFront(SListNode** pphead, SListNode* pos, SLTDateType x)
{
	assert(pphead);
	assert(*pphead);
	assert(pos);

	SListNode* new = BuySListNode(x);
	new->next = pos;
	if (pos == *pphead)
	{
		*pphead = new;
	}
	else
	{
		// 找pos结点
		SListNode* cur = *pphead;
		while (cur->next != pos)
		{
			cur = cur->next;
		}
		cur->next = new;
	}
}

void SListEraseAfter(SListNode* pos)
{
	assert(pos);
	assert(pos->next);

	SListNode* next = pos->next;
	pos->next = next->next;
	free(next);
	next = NULL;
}

void SLTErase(SListNode** pphead, SListNode* pos)
{
	assert(pphead);
	assert(*pphead);
	assert(pos);

	if (*pphead == pos)
	{
		if (pos->next == NULL)
		{
			*pphead = NULL;
		}
		else
		{
			*pphead = pos->next;
		}
		free(pos);
		pos = NULL;
	}
	else
	{
		// 找pos之前的结点
		SListNode* prev = *pphead;
		while (prev->next != pos)
		{
			prev = prev->next;
		}
		prev->next = pos->next;
		free(pos);
		pos = NULL;
	}
}

void SListDestroy(SListNode* plist)
{
	assert(plist);

	while (plist->next)
	{
		SListEraseAfter(plist->next);
	}
	
	free(plist);

}