#define _CRT_SECURE_NO_WARNINGS 1
#include"Slist.h"

void TestSList()
{
	SListNode* s = NULL;
	SListPushBack(&s, 1);
	SListPushBack(&s, 2);
	SListPushBack(&s, 3);
	SListPushBack(&s, 4);
	SListPushBack(&s, 5);

	SListPrint(s);

	SListPushFront(&s, 6);
	SListPushFront(&s, 7);
	SListPushFront(&s, 8);
	SListPushFront(&s, 9);

	SListPrint(s);

	SListPopBack(&s);
	SListPopBack(&s);
	SListPopBack(&s);

	SListPrint(s);

	SListPopFront(&s);
	SListPopFront(&s);
	SListPopFront(&s);

	SListPrint(s);


	SLTInsertFront(&s, SListFind(s, 1), 8);

	SListPrint(s);
}

struct SListNode* removeElements(struct SListNode* head, int val) {
	if (!head)
	{
		return NULL;
	}
	struct SListNode* cur = head, * del = NULL;
 	while (cur && cur->next)
	{
		if (cur->next->data == val)
		{
			del = cur->next;
			
			cur->next = del->next;
			free(del);
		}
		else
			cur = cur->next;
	}
	if (head->data == val)
	{
		del = head;
		head = head->next;
		free(del);
	}
	return head;
}

TestSList1()
{
	SListNode* s = NULL;

	SListPushBack(&s, 6);
	SListPushBack(&s, 6);
	SListPushBack(&s, 6);

	SListPushBack(&s, 6);


	SListPrint(s);

	SListPrint(removeElements(s, 6));
}

SListNode* mergeTwoLists(SListNode* list1, SListNode* list2) {
	SListNode* head = NULL, * tail = NULL;
	while (list1 && list2)
	{
		if (head == NULL)
		{
			if (list1->data < list2->data)
			{
				tail = head = list1;
				list1 = list1->next;
			}
			else
			{
				tail = head = list2;
				list2 = list2->next;
			}
		}
		else
		{
			if (list1->data < list2->data)
			{
				tail->next = list1;
				list1 = list1->next;
			}
			else
			{
				tail->next = list2;
				list2 = list2->next;
			}
			tail = tail->next;
		}
		
		
	}
	// list1与list2中有一表为空链表
	if (!head)
	{
		head = list1 ? list1 : list2;
	}
	// 判断list1与list2中谁有剩余
	else
	{
		// head不为空，tail一定有赋值
		tail->next = list1 ? list1 : list2;
	}
	return head;
}

TestSList2()
{
	SListNode* s1 = NULL;
	SListNode* s2 = NULL;

	SListPushBack(&s1, 1);
	SListPushBack(&s1, 2);
	SListPushBack(&s1, 4);


	SListPrint(s1);
	SListPushBack(&s2, 1);
	SListPushBack(&s2, 3);
	SListPushBack(&s2, 4);


	SListPrint(s2);

	SListPrint(mergeTwoLists(s1, s2));
}

SListNode* partition(SListNode* head, int x) {
	SListNode* tmp = (SListNode*)malloc(sizeof(SListNode));
	SListNode* cur = head, * tail1 = NULL, * tail2 = tmp;
	// head用于保存小于x的值，同时保持顺序
	head = NULL;
	//将比x大的值尾插到tmp后，随后将此链表接到原表后
	while (cur)
	{
		if (cur->data < x)
		{
			if (!head)
			{
				tail1 = head = cur;
			}
			else
			{
				tail1->next = cur;
				tail1 = tail1->next;
			}
		}
		else
		{
			tail2->next = cur;
			tail2 = tail2->next;
		}
		cur = cur->next;
	}
	tail2->next = NULL;
	if (tail1)
		tail1->next = tmp->next;
	// 存在值全大于x的情况
	else
		head = tmp->next;
	free(tmp);
	return head;
}

TestSList3()
{
	SListNode* s = NULL;
	SListPushBack(&s, 1);
	//SListPushBack(&s, 4);
	//SListPushBack(&s, 3);
	//SListPushBack(&s, 2);
	//SListPushBack(&s, 5);
	//SListPushBack(&s, 2);

	SListPrint(s);

	SListPrint(partition(s, 0));
}

int main()
{
	//TestSList3();
	SListNode* a = NULL;
	SListPushBack(&a, 1);
	SListPushBack(&a, 2);
	SListPushBack(&a, 3);

	SListNode* b = a->next;
	SListNode* c = a;
	a= a->next = b->next;
}