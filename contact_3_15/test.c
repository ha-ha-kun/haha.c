#define _CRT_SECURE_NO_WARNINGS 1

#include"contact.h"

int main()
{
	int input = 0;
	Contact con;
	Init_Contact(&con);
	do
	{
		menu();
		printf("请输入选项：");
		scanf("%d", &input);
		switch (input)
		{
		case ADD:
			Add_Contact(&con);
			break;
		case DEL:
			Del_By_Name(&con);
			break;
		case SEARCH:
			Search(&con);
			break;
		case MODIFY:
			Modify(&con);
			break;
		case SORT:
			Sort(&con);
			break;
		case PRINT:
			PrintC(&con);
			break;
		case EXIT:
			Save_Contact(&con);
			Exit_Contact(&con);
			printf("退出通讯录");
			break;
		default:
			printf("输入错误，请重新输入!\n");
			break;
		}
	} while (input);
}