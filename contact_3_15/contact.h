#pragma once

#include<stdio.h>
#include<string.h>
#include<assert.h>
#include<stdlib.h>

//枚举提高代码可读性
enum Option
{
	EXIT,
	ADD,
	DEL,
	SEARCH,
	MODIFY,
	SORT,
	PRINT
};
//类型声明，结构体各成员的大小
#define NAME_MAX 20
#define SEX_MAX 5
#define TEL_MAX 15
#define ADDR_MAX 30

//联系表大小-静态版
#define MAX 1000


//联系人结构体
typedef struct Peo
{
	char name[NAME_MAX];
	char sex[SEX_MAX];
	int age;
	char tel[TEL_MAX];
	char addr[ADDR_MAX];
}Peo;

////联系表结构体静态版
//typedef struct Contact
//{
//	Peo data[MAX];
//	int sz;
//}Contact;
//联系表结构体
typedef struct Contact
{
	Peo* data;
	int sz;
	int capacity;//容量
}Contact;

//菜单
void menu();

//初始化
void Init_Contact(Contact* con);

//增加联系人
void Add_Contact(Contact* con);

//删除联系人
void Del_By_Name(Contact* con);

//搜索联系人
void Search(Contact* con);

//打印通讯录
void PrintC(Contact* con);

//修改联系人
void Modify(Contact* con);

//排序
void Sort(Contact* con);

//退出
void Exit_Contact(Contact* con);

//保存通讯录
void Save_Contact(Contact* con);