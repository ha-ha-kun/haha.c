#define _CRT_SECURE_NO_WARNINGS 1

#include"contact.h"

//菜单
void menu()
{
	printf("************************************\n");
	printf("******  1、add     2、del     ******\n");
	printf("******  3、search  4、modify  ******\n");
	printf("******  5、sort    6、print   ******\n");
	printf("******  0、exit               ******\n");
	printf("************************************\n");
}

////初始化静态
//void Init_Contact(Contact* con)
//{
//	assert(con);
//	con->sz = 0;
//	memset(con->data, 0, sizeof(con->data));
//}

//初始化动态
//初始为3，后续每次增加2
//void Init_Contact(Contact* con)
//{
//	assert(con);
//	con->sz = 0;
//	con->capacity = 3;
//	con->data = (Peo*)malloc(sizeof(Peo) * con->capacity);
//}


////增加联系人静态
//void Add_Contact(Contact* con)
//{
//	assert(con);
//	if (con->sz == MAX)
//	{
//		printf("通讯录已满！不可添加");
//		return;
//	}
//	printf("请输入名字：");
//	scanf("%s", con->data[con->sz].name);
//	printf("请输入性别：");
//	scanf("%s", con->data[con->sz].sex);
//	printf("请输入年龄：");
//	scanf("%d", &(con->data[con->sz].age));
//	printf("请输入电话：");
//	scanf("%s", con->data[con->sz].tel);
//	printf("请输入地址：");
//	scanf("%s", con->data[con->sz].addr);
//	con->sz++;
//	printf("添加成功!\n");
//}



// 扩容
void Enlarge(Contact* con)
{
	con->capacity += 2;
	Peo* tmp = NULL;
	tmp = (Peo*)realloc(con->data, (con->capacity) * sizeof(Peo));
	if (tmp != NULL)
	{
		con->data = tmp;
		printf("扩容成功\n");
	}
	else
	{
		printf("扩容失败\n");
		con->capacity -= 2;
	}
}

void Load_Contact(Contact* con, FILE* pf)
{
	Peo tmp = { 0 };
	while (EOF != fscanf(pf, "%s %s %d %s %s ", tmp.name, tmp.sex, &(tmp.age), tmp.tel, tmp.addr))
	{
		if (con->sz == con->capacity)
		{
			//扩容
			Enlarge(con);
		}
		con->data[con->sz] = tmp;
		con->sz++;
	}
}
//文件版本
void Init_Contact(Contact* con)
{
	assert(con);
	FILE* pf = NULL;
	pf = fopen("contact.txt", "r");
	if (pf == NULL)
	{
		perror("Init_Contact::fopen");
		return;
	}
	con->sz = 0;
	con->capacity = 3;
	con->data = (Peo*)malloc(sizeof(Peo) * con->capacity);
	Load_Contact(con, pf);
	fclose(pf);
	pf = NULL;
}

//增加联系人动态
void Add_Contact(Contact* con)
{
	assert(con);
	if (con->sz == con->capacity)
	{
		//扩容
		Enlarge(con);
	}
	printf("请输入名字：");
	scanf("%s", con->data[con->sz].name);
	printf("请输入性别：");
	scanf("%s", con->data[con->sz].sex);
	printf("请输入年龄：");
	scanf("%d", &(con->data[con->sz].age));
	printf("请输入电话：");
	scanf("%s", con->data[con->sz].tel);
	printf("请输入地址：");
	scanf("%s", con->data[con->sz].addr);
	con->sz++;
	printf("添加成功!\n");
}

//以名字搜索
int Search_By_Name(Contact* con, char* cmp)
{
	int i = 0;
	for (i = 0; i < con->sz; i++)
	{
		if (strcmp(cmp, con->data[i].name) == 0)
		{
			return i;
		}
	}
	return -1;
}

////按名字删除静态
//void Del_By_Name(Contact* con)
//{
//	assert(con);
//	if (con->sz == 0)
//	{
//		printf("通讯录为空！不可删除\n");
//		return;
//	}
//	char cmp[20] = { 0 };
//	printf("请输入要删除的人：");
//	scanf("%s", cmp);
//	int x = Search_By_Name(con, cmp);
//	if (x != -1)
//	{
//		for (; x < con->sz; x++)
//		{
//			con->data[x] = con->data[x + 1];
//		}
//		con->sz--;
//		printf("删除成功！\n");
//	}
//	else
//	{
//		printf("要删除的人不存在!\n");
//	}
//}

//按名字删除动态
void Del_By_Name(Contact* con)
{
	assert(con);
	if (con->sz == 0)
	{
		printf("通讯录为空！不可删除\n");
		return;
	}
	char cmp[20] = { 0 };
	printf("请输入要删除的人：");
	scanf("%s", cmp);
	int x = Search_By_Name(con, cmp);
	if (x != -1)
	{
		for (; x < con->sz; x++)
		{
			con->data[x] = con->data[x + 1];
		}
		con->sz--;
		printf("删除成功！\n");
		if (con->capacity - con->sz == 2)
		{
			con->capacity -= 2;
			con->data = (Peo*)realloc(con->data, con->capacity);
		}
	}
	else
	{
		printf("要删除的人不存在!\n");
	}
}


//打印指定下标联系人
void PrintP(const Contact* con, int i)
{
	printf("%20s %5s %5d %15s %30s\n", con->data[i].name, con->data[i].sex, con->data[i].age, con->data[i].tel, con->data[i].addr);
}

//查找
void Search(const Contact* con)
{
	assert(con);
	if (con->sz == 0)
	{
		printf("通讯录为空，不可查找\n");
	}
	int i = 0;
	char cmp[20] = { 0 };
	printf("请输入要查找的人：");
	scanf("%s", cmp);
	i = Search_By_Name(con, cmp);
	if (i != -1)
	{
		printf("信息如下：\n");
		printf("%20s %5s %5s %15s %30s\n", "姓名", "性别", "年龄", "电话", "住址");
		PrintP(con, i);
	}
	else
	{
		printf("查找的人不存在！\n");
	}
}

//修改选项
void Mmenu()
{
	printf("************************************\n");
	printf("*******  1、Name   2、Sex    *******\n");
	printf("*******  3、Age    4、Tel    *******\n");
	printf("*******  5、Addr   0、Exit    ******\n");
	printf("************************************\n");
}


//修改选项
enum Option2
{
	Exit,
	Name,
	Sex,
	Age,
	Tel,
	Addr
};

//修改函数
void Modify(Contact* con)
{
	assert(con);
	if (con->sz == 0)
	{
		printf("通讯录为空！\n");
	}
	char cmp[20] = { 0 };
	printf("请输入要修改的人的名字：");
	scanf("%s", cmp);
	int i = Search_By_Name(con, cmp);
	if (i == -1)
	{
		printf("此人不存在！\n");
	}
	else
	{
		int input = 0;
		do
		{
			Mmenu();
			printf("%20s %5s %5s %15s %30s\n", "姓名", "性别", "年龄", "电话", "住址");
			PrintP(con, i);
			printf("请输入修改选项：");
			scanf("%d", &input);
			switch (input)
			{
			case Name:
				printf("请输入新名字：");
				scanf("%s", con->data[i].name);
				break;
			case Sex:
				printf("请输入性别：");
				scanf("%s", con->data[i].sex);
				break;
			case Age:
				printf("请输入年龄：");
				scanf("%d", con->data[i].age);
				break;
			case Tel:
				printf("请输入新电话：");
				scanf("%s", con->data[i].tel);
				break;
			case Addr:
				printf("请输入新住址：");
				scanf("%s", con->data[i].addr);
				break;
			case Exit:
				printf("退出修改\n");
				break;
			default:
				break;
			}

		} while (input);
	}
}

//按名字比大小
int cmp_by_name(const void* e1, const void* e2)
{
	return strcmp(((Peo*)e1)->name, ((Peo*)e2)->name);
}

//按年龄比大小
int cmp_by_age(const void* e1, const void* e2)
{
	return ((Peo*)e1)->age - ((Peo*)e2)->age;
}

//排序选项
void Smenu()
{
	printf("************************************\n");
	printf("*******  1、Name   2、Age    *******\n");
	printf("************************************\n");
}
//通讯录排序
void Sort(Contact* con)
{
	assert(con);
	int input = 0;
	Smenu();
	printf("请选择要排序的方式：");
	scanf("%d", &input);
	int (*pc[3])(const void* e1, const void* e2) = { 0 , cmp_by_name, cmp_by_age };
	qsort(con->data, con->sz, sizeof(Peo), pc[input]);
}

//打印通讯录
void PrintC(const Contact* con)
{
	assert(con);
	if (con->sz == 0)
	{
		printf("通讯录为空！\n");
	}
	int i = 0;
	printf("%20s %5s %5s %15s %30s\n", "姓名", "性别", "年龄", "电话", "住址");
	for (i = 0; i < con->sz; i++)
	{
		PrintP(con, i);
	}
	printf("*****   打印完成   *****\n");
}

void Save_Contact(Contact* con)
{
	FILE* pf;
	pf = fopen("contact.txt", "w");
	if (pf == NULL)
	{
		perror("Save_Contact::fopen::");
		return;
	}
	int i = 0;
	for (i = 0; i < con->sz; i++)
	{
		fprintf(pf, "%s %s %d %s %s ", (con->data + i )->name, (con->data + i)->sex, (con->data + i)->age, (con->data + i)->tel, (con->data + i) ->addr);
	}
	fclose(pf);
	pf = NULL;
}

//退出通讯录
void Exit_Contact(Contact* con)
{
	free(con->data);
	con->data = NULL;
	con->capacity = 0;
	con->sz = 0;
}