#pragma once

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define ROW 9
#define COL 9

#define ROWS 11
#define COLS 11

//游戏难度
#define EASY 10


//初始化数组，初始arr数组中的值为set
void Initarr(char arr[ROWS][COLS], int row, int col, char set);

//打印“雷区”
void Display(char arr[ROWS][COLS], int row, int col);

//随机布置雷
void Setmine(char mine[ROWS][COLS], int row, int col, int count);

//扫雷
void Sweep(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col, int count);

