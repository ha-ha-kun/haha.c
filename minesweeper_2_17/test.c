#define _CRT_SECURE_NO_WARNINGS 1

#include"game.h"

int main()
{
	int x = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		scanf("%d", &x);
		switch (x)
		{
		case 1:
			printf("游戏开始\n");
			game();
			break;
		case 0:
			printf("游戏退出\n");
			break;
		default:
			printf("输入错误，请重新输入\n");
			break;
		}
	} while (x);
}