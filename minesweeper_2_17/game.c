#define _CRT_SECURE_NO_WARNINGS 1

#include"game.h"
//具体函数的实现

//选择菜单
void menu()
{
	printf("******************\n");
	printf("***** 1、开始 ****\n");
	printf("***** 0、退出 ****\n");
	printf("******************\n");
}

//初始化数组
void Initarr(char arr[ROWS][COLS], int row, int col, char set)
{
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			arr[i][j] = set;
		}
	}
}

//打印“雷区”,由于第一行、最后一行、第一列与最后一列作为辅助作用，因此不予打印，只打印中间9行
void Display(char arr[ROWS][COLS], int row, int col)
{
	printf("------- 扫雷 ------\n");
	printf("0 1 2 3 4 5 6 7 8 9\n");//打印列标
	for (int i = 1; i < row + 1; i++)
	{
		printf("%d ", i);//打印行标
		for (int j = 1; j < col + 1; j++)
		{
			printf("%c ", arr[i][j]);
		}
		printf("\n");
	}
	printf("------- 扫雷 ------\n");
}

//随机布置雷
void Setmine(char mine[ROWS][COLS], int row, int col, int count)
{
	int x = 0;
	int y = 0;
	while (count)
	{
		//mine为11×11大小，首行末行与首列末列均不布置雷
		x = rand() % row + 1;
		y = rand() % col + 1;
		if (mine[x][y] == '0')
		{
			mine[x][y] = '1';
			count--;
		}
	}
}

//获取周边的雷的个数
static int get_mine(char mine[ROWS][COLS], int x, int y)
{
	int count = 0;
	for (int i = -1; i < 2; i++)
	{
		for(int j = -1; j < 2; j++)
			if (mine[x + i][y + j] == '1')
			{
				count++;
			}
	}
	return count;
}

//展开函数
static void unfold(char mine[ROWS][COLS], char show[ROWS][COLS], int x, int y)
{
	if (x == 0 || x == ROW + 1 || y == 0 || y == COL + 1)
		return;
	if (show[x][y] == '*')
	{
		int ret = get_mine(mine, x, y);
		if (ret == 0)
		{
			show[x][y] = ' ';
			unfold(mine, show, x -1, y -1);
			unfold(mine, show, x - 1, y);
			unfold(mine, show, x - 1, y + 1);
			unfold(mine, show, x, y - 1);
			unfold(mine, show, x, y + 1);
			unfold(mine, show, x + 1, y - 1);
			unfold(mine, show, x + 1, y);
			unfold(mine, show, x + 1, y + 1);

			
		}
		else
		{
			show[x][y] = ret + '0';
		}
		
	}
	
}
//扫雷可能出现的情况：
// 1、坐标非法->重新输入
// 2、坐标合法->A、踩雷->游戏结束
//              B、未踩雷->a、周边有雷->显示雷的数量
//                         b、周边无雷->大面积展开
//              C、该位置已经被扫过->坐标非法
void Sweep(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col, int count)
{
	int x = 0, y = 0;
	while (1)
	{
		printf("请输入坐标：\n");
		scanf("%d %d", &x, &y);
		if (x < 1 || x > row || y < 1 || y > col)
		{
			printf("坐标非法，请重新输入：\n");
		}
		else if (mine[x][y] == '1')
		{
			printf("此处有雷，游戏结束\n");
			Display(mine, row, col);
			break;
		}
		else if (show[x][y] != '*')
		{
			printf("该位置已被扫过，请重新输入：\n");
		}
		else
		{
			//周边无雷
			if (get_mine(mine, x, y) == 0)
			{
				unfold(mine, show, x, y);
				Display(show, row, col);
			}
			//周边有雷
			else
			{
				show[x][y] = get_mine(mine, x, y) + '0';
				Display(show, row, col);
			}
		}
		//判断是否完成
		int win = 0;
		for (int i = 1; i < row + 1; i++)
		{
			for (int j = 1; j < col + 1; j++)
			{
				if (show[i][j] == '*')
					win++;
			}
		}
		if (win == EASY)
		{
			printf("扫雷成功！\n");
			Display(mine, row, col);
			break;
		}
	}
		
}

void game()
{
	// mine数组用以存放雷
	char mine[ROWS][COLS] = { 0 };
	// show数组用以存放扫出的信息
	char show[ROWS][COLS] = { 0 };
	//初始化mine数组为字符0，show数组为字符'*'
	Initarr(mine, ROWS, COLS, '0');
	//Display(mine, ROW, COL);//检测初始化效果
	Initarr(show, ROWS, COLS, '*');
	//Display(show, ROW, COL);//检测初始化效果
	Setmine(mine, ROW, COL, EASY);
	Display(mine, ROW, COL);//查看雷的布置位置
	Display(show, ROW, COL);
	Sweep(mine, show, ROW, COL, EASY);
}