#define _CRT_SECURE_NO_WARNINGS 1

int main()
{
    int[7] = { 1, 2, 3, 4, 5, 6, 7 };

}

void rotate(int* nums, int numsSize, int k) {
    int newA[numsSize];
    // 创建新数组
    for (int i = 0; i < numsSize; i++)
    {
        newA[(i + k) % numsSize] = nums[i];
    }
    // 赋值
    for (int i = 0; i < numsSize; i++)
    {
        nums[i] = newA[i];
    }
}