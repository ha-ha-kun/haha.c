#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<stdlib.h>

// 给定一个整数数组 nums，将数组中的元素向右轮转 k 个位置，其中 k 是非负数。

// 定义数组存储转后的元素，再赋值回去
void rotate(int* nums, int numsSize, int k) {
	int* tmp = (int*)malloc(numsSize * sizeof(int));
	int i = 0;
	for (i = 0; i < numsSize; i++)
	{
		tmp[i] = nums[(i + k) % numsSize];
	}
	for (i = 0; i < numsSize; i++)
	{
		nums[i] = tmp[i];
	}
	free(tmp);
}

int main()
{
	int n[] = { 1,2,3,4,5,6,7 };
	int k = 3;
	rotate(n, sizeof(n) / sizeof(n[0]), k);
	return 0;
}