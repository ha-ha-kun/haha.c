#pragma once

#include<stdio.h>
#include<assert.h>
#include<stdlib.h>

#define SLDATATYPE int

typedef struct SLTNode
{
	SLDATATYPE data;
	struct SLTNode* next;
}SLTNode;

void SListPopBack(SLTNode** pphead);
