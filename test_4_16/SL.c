#define _CRT_SECURE_NO_WARNINGS 1
#include"SL.h"

void SListPopBack(SLTNode** pphead)
{
	assert(*pphead);// 判空
	// 一个结点
	if ((*pphead)->next == NULL)
	{
		free(*pphead);
		*pphead = NULL;
	}
	// 多个结点
	else
	{
		SLTNode* tail = *pphead, * pre = NULL;
		while (tail->next)
		{
			pre = tail; // 保存前一个结点
			tail = tail->next;
		}
		free(tail);
		pre->next = NULL;
	}
}
	