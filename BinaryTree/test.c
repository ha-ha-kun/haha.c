#define _CRT_SECURE_NO_WARNINGS 1
#include"Binary.h"

void Test1()
{
	char a[] = { "ABD##E#H##CF##G##" };
	int i = 0;
	BTNode* tree = NULL;

	tree = BinaryTreeCreate(a, sizeof(a) / sizeof(char), &i);

	int size1 = BinaryTreeSize(tree);

	int size2 = BinaryTreeLeafSize(tree);
	int size3 = BinaryTreeLevelKSize(tree,1);
	int size4 = BinaryTreeLevelKSize(tree, 2);
	int size5 = BinaryTreeLevelKSize(tree, 3);
	int size6 = BinaryTreeLevelKSize(tree, 4);
	int size7 = BinaryTreeLevelKSize(tree, 5);


	BTNode* n1 = BinaryTreeFind(tree, 'A');
	BTNode* n2 = BinaryTreeFind(tree, 'E');
	BTNode* n3 = BinaryTreeFind(tree, 'J');

	BinaryTreePrevOrder(tree);
	printf("\n");
	BinaryTreeInOrder(tree);
	printf("\n");
	BinaryTreePostOrder(tree);
	printf("\n");
	BinaryTreeLevelOrder(tree);
	printf("\n");


	int s = BinaryTreeComplete(tree);

	char b[] = { "ABD##E##CF##G##" };

	BTNode* tree1 = NULL;
	i = 0;
	tree1 = BinaryTreeCreate(b, sizeof(b) / sizeof(char), &i);
	int s1 = BinaryTreeComplete(tree1);
	

	int q = 1;
	BinaryTreeDestory(&tree);

	

}

int main()
{
	Test1();


	return 0;
}