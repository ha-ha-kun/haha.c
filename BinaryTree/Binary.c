#define _CRT_SECURE_NO_WARNINGS 1
#include"Binary.h"
#include"Queue.h"

BTNode* BuyNode(BTDataType x)
{
	BTNode* tmp = (BTNode*)malloc(sizeof(BTNode));
	if (!tmp)
	{
		perror("malloc fail");
		exit(-1);
	}

	tmp->data = x;
	tmp->left = tmp->right = NULL;

	return tmp;
}

BTNode* TreeCreate(BTNode* root, BTDataType* a, int* pi, int n)
{
	// #号返回NULL
	if (a[(*pi)] == '#')
	{
		(*pi)++;
		return NULL;
	}
	root = BuyNode(a[(* pi)++]);
	root->left = TreeCreate(root->left, a, pi, n);
	root->right = TreeCreate(root->right, a, pi, n);

	return root;
	
}

BTNode* BinaryTreeCreate(BTDataType* a, int n, int* pi)
{
	// 创建根节点，随后
	BTNode* ret = NULL;
	ret = TreeCreate(ret, a, pi, n);
	
	return ret;
}

void BinaryTreeDestory(BTNode** root)
{
	if (*root == NULL)
		return;
	BinaryTreeDestory(&((*root)->left));
	BinaryTreeDestory(&((*root)->right));
	free(*root);
	*root = NULL;

}

int BinaryTreeSize(BTNode* root)
{
	return root == NULL ? 0 : BinaryTreeSize(root->left) + BinaryTreeSize(root->right) + 1;
}

int BinaryTreeLeafSize(BTNode* root)
{
	if (root == NULL)
		return 0;
	if (!root->left && !root->right)
		return 1;

	return BinaryTreeLeafSize(root->left) + BinaryTreeLeafSize(root->right);
}

int BinaryTreeLevelKSize(BTNode* root, int k)
{
	if (k == 1 && root)
	{
		return 1;
	}
	if (root == NULL)
	{
		return 0;
	}
	int left =  BinaryTreeLevelKSize(root->left, k - 1);
	int right = BinaryTreeLevelKSize(root->right, k - 1);

	return left + right;
}

BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	if (root == NULL)
		return NULL;
	if (root->data == x)
		return root;
	BTNode* ret = BinaryTreeFind(root->left, x);
	// 在左边找到了
	if (ret)
		return ret;
	ret = BinaryTreeFind(root->right, x);
	// 在右边找到或没找到都可直接返回
	return ret;
}

void BinaryTreePrevOrder(BTNode* root)
{
	if (root == NULL)
		return;
	printf("%c ", root->data);
	BinaryTreePrevOrder(root->left);
	BinaryTreePrevOrder(root->right);
}
void BinaryTreeInOrder(BTNode* root)
{
	if (root == NULL)
		return;
	BinaryTreePrevOrder(root->left);
	printf("%c ", root->data);
	BinaryTreePrevOrder(root->right);
}

void BinaryTreePostOrder(BTNode* root)
{
	if (root == NULL)
		return;
	BinaryTreePrevOrder(root->left);
	BinaryTreePrevOrder(root->right);
	printf("%c ", root->data);
}

void LevelOrder(Queue* q, BTNode* root)
{
	if (root == NULL)
		return;
	// 队空则入队
	if (root == NULL)
		return;
	QueuePush(q, root);
	// 否则出队
	while (!QueueEmpty(q))
	{
		BTNode* r = QueueFront(q);
		QueuePop(q);
		printf("%c ", r->data);
		if (r->left)
			QueuePush(q, r->left);
		if (r->right)
			QueuePush(q, r->right);
	}
	
}

void BinaryTreeLevelOrder(BTNode* root)
{
	Queue q;
	QueueInit(&q);

	LevelOrder(&q, root);

	QueueDestroy(&q);
}

//int TreeComplete(Queue* q, BTNode* root)
//{
//	if (root == NULL)
//		return;
//	// 空则入队
//	if (QueueEmpty(q))
//	{
//		QueuePush(q, root);
//	}
//	// 否则出队
//	BTNode* r = QueueFront(q);
//	QueuePop(q);
//	printf("%c ", r->data);
//	if (r->left)
//		QueuePush(q, r->left);
//	if (r->right)
//		QueuePush(q, r->right);
//
//	LevelOrder(q, q->front);
//}


int BinaryTreeComplete(BTNode* root)
{
	Queue q;
	QueueInit(&q);
	// 根入队
	if (root == NULL)
	{
		return true;
	}
	QueuePush(&q, root);
	// 将树中节点依次入队，遇到空则停止入队
	while (!QueueEmpty(&q))
	{
		BTNode* f = QueueFront(&q);
		if (f == NULL)
		{
			break;
		}
		QueuePop(&q);
		if(root->left)
			QueuePush(&q, f->left);
		if(root->right)
			QueuePush(&q, f->right);
	}

	// 判断剩余的元素中是否非空元素
	while (!QueueEmpty(&q))	
	{
		BTNode* f = QueueFront(&q);
		QueuePop(&q);

		if (f != NULL)
		{
			QueueDestroy(&q);
			return false;
		}
	}
	QueueDestroy(&q);
	return true;
}