#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
//打印乘法口诀表
void print_table(int i)
{
	int m = 0;
	int n = 0;
	for (m = 1; m <= i; m++)
	{
		for (n = 1; n <= m; n++)
		{
			printf("%d*%d=%-2d ", n, m, m * n);//%2d打印两位，右对齐，数值不够用空格填充；采用%-2d;即可以实现左对齐；
		}
		printf("\n");
	}
}
int main()
{
	int i = 0;
	scanf("%d", & i);
	print_table(i);
	return 0;
}