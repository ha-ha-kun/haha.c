#define _CRT_SECURE_NO_WARNINGS 1

////有一个数字矩阵，矩阵的每行从左到右是递增的，矩阵从上到下是递增的，请编写程序在这样的矩阵中查找某个数字是否存在。
////杨氏矩阵求解，从右上角往左下角比较，所求值大于比较值则往下走，反之往左走。
//
//#include<stdio.h>
//void find(int(*p)[4], int x, int i, int j)
//{
//	//判断坐标是否合法
//	if (i >= 0 && i < 4 && j >= 0 && j < 4)
//	{
//		if (x < p[i][j])
//		{
//			//小于往左走
//			find(p, x, i, j - 1);
//		}
//		else if (x > p[i][j])
//		{
//			//大于往下走
//			find(p, x, i + 1, j);
//		}
//		else if (x == p[i][j])
//		{
//			printf("坐标为：%d %d", i, j);
//		}
//		else
//		{
//			printf("找不到");
//		}
//	}	
//}
//
//int main()
//{
//	int arr[4][4] = { {1,5,7,9}, {4, 6, 10, 15}, {8, 11, 12, 19}, {14, 16, 18, 21} };
//	int x = 0;
//	int i = 0, j = 3;
//	scanf("%d", &x);
//	find(arr, x, i, j);
//	return 0;
//}

////实现一个函数，可以左旋字符串中的k个字符。例如：ABCD左旋一个字符得到BCDA ABCD左旋两个字符得到CDAB
//void lrotate(char* p, int sz, int x)
//{
//	char tmp = 0;
//	while (x)
//	{
//		//暂存第一位的值
//		tmp = p[0];
//		//字符串左移
//		int i = 0;
//		for (i = 0; i < sz - 2; i++)
//		{
//			p[i] = p[i + 1];
//		}
//		//字符串最后一位修改
//		p[i] = tmp;
//		x--;
//	}
//}
//#include<stdio.h>
//int main()
//{
//	char arr[] = "abcd";
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int n = 0;
//	scanf("%d", &n);
//	lrotate(arr, sz, n);
//	printf("%s", arr);
//	return 0;
//}