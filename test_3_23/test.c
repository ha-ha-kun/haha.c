#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stddef.h>

//// 定义宏，交换一个整数的二进制奇偶位
//// 提取偶数位 0×aaaa aaaa aaaa aaaa 按位与& 得到偶数位 右移1位 <<1 变成奇数位
//// 提取奇数位 0×5555 5555 5555 5555 按位与& 得到奇数位 左移1位 >>1 变成偶数位
//// 二者或 完成交换
//#define SWAPB(x) ((0xaaaaaaaaaaaaaaaa & (x)) >> 1)|((0x5555555555555555 & (x)) << 1)
//
//
//int main()
//{
//	int a = -1;
//	int b = SWAPB(a);
//	printf("%d %d\n", a, b);
//	return 0;
//}



//#define MAX 100;
//
//int main()
//{
//	int a = 0;
//	if (1)
//		a = MAX
//	else
//		a = 1;
//}


//#define ADD (x, y) ((x)+(y))
//
//int main()
//{
//	int a = 2;
//	int b = 3;
//	int c = 4;
//	printf("%d\n", c * ADD(a, b));
//	printf("%d\n", SQUARE(a + 1, b));
//}

//#define ADD(x, y) ((x)+(y))
//#define SQUARE(x, y) ((x) * (y))
//
//int main()
//{
//	int a = 2;
//	int b = 3;
//	int c = 4;
//	printf("%d\n", c * ADD(a, b));
//	printf("%d\n", SQUARE(a + 1, b));
//}


//写一个宏，计算结构体中某变量相对于首地址的偏移，并给出说明

#define MY_offsetof(str, m)  (int)&(((str*)0)->m)

int main()
{
	typedef struct Stu
	{
		char a;//偏移量0
		int b;//4
		double c;//8
		char d;//16
		short e;//18
	}Stu;
	printf("%d\n", offsetof(Stu, a));
	printf("%d\n", offsetof(Stu, b));
	printf("%d\n", offsetof(Stu, c));
	printf("%d\n", offsetof(Stu, d));
	printf("%d\n", offsetof(Stu, e));
	printf("%d\n", MY_offsetof(Stu, a));
	printf("%d\n", MY_offsetof(Stu, b));
	printf("%d\n", MY_offsetof(Stu, c));
	printf("%d\n", MY_offsetof(Stu, d));
	printf("%d\n", MY_offsetof(Stu, e));

	return 0;
}