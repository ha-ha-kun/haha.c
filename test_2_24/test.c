#define _CRT_SECURE_NO_WARNINGS 1

//#include<stdio.h>
//struct stu
//{
//	int num;
//	char name[10];
//	int age;
//};
//
//void fun(struct stu* p)
//{
//	printf("%s\n", (*p).name);
//	return;
//}
//
//int main()
//{
//	struct stu students[3] = 
//	{ 
//		{9801,"zhang",20},
//		{9802,"wang",19},
//		{9803,"zhao",18} 
//	};
//	fun(students + 1);
//	return 0;
//}

//喝汽水，1瓶汽水1元，2个空瓶可以换一瓶汽水，给20元，可以多少汽水
#include<stdio.h>
//根据钱值判断汽水数目
//1元一瓶—>原值 两空瓶换汽水—>汽水数/2
//递归解决，
//int coke(int x)
//{
//	return x + cokenum(x);
//}
////判断瓶子
//int cokenum(int x)
//{
//	if (x == 1)
//	{
//		return 0;
//	}
//	//只有两个瓶返回1
//	else if (x == 2)
//	{
//		return 1;
//	}
//	//偶数瓶进入递归
//	else if (x % 2 == 0)
//	{
//		return x / 2 + cokenum(x / 2);
//	}
//	//奇数瓶进入递归，需加上剩下的一个瓶
//	else
//	{
//		return x / 2 + cokenum(x / 2 + 1);
//	}
//}
//int main()
//{
//	int x = 0;
//	scanf("%d", &x);
//	printf("%d", x + cokenum(x));
//	return 0;
//}

// 实现方法2
//#include<stdio.h>
//int main()
//{
//	int x = 0;
//	int coke = 0, bottle = 0;
//	scanf("%d", &x);
//	while (x)
//	{
//		x--;
//		coke++;
//		bottle++;
//		if (bottle == 2)//瓶子数等于2时就换汽水，换了之后瓶数-1；
//		{
//			bottle--;
//			coke++;
//		}
//	}
//	printf("%d", coke);
//	return 0;
//}

//实现方法3
//#include<stdio.h>
//int main()
//{
//	int x = 0;
//	scanf("%d", &x);
//	printf("%d", 2 * x - 1);
//}
//#include <stdio.h>
//int main()
//{
//    int i = 0;
//    int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//    for (i = 0; i <= 12; i++)
//    {
//        arr[i] = 0;
//        printf("hello bit\n");
//    }
//    return 0;
//}

//模拟实现库函数strlen
//#include<stdio.h>
//int my_strlen(char* a)
//{
//	int i = 0;
//	for (i = 0; *(a + i) != '\0'; i++);
//	return i;
//}
//int main()
//{
//	char arr[] = {"hey123"};
//	printf("%d", my_strlen(arr));
//}

////模拟实现库函数strcpy
////复制字符到另一个字符，返回指向目标字符的指针
//char* my_strlen(char* a, const char* b)
//{
//	int i = 0, j = 0, x = 0;
//	for (i = 0; a[i] != '\0'; i++);
//	for (j = 0; b[j] != '\0'; j++);
//	for (x = 0; x <= j; x++)
//	{
//		a[i + x] = b[x];
//	}
//	return a;
//}
//
//int main()
//{
//	char a[20] = { "hahaha" };
//	char b[20] = { "hehehe" };
//	printf("%s",my_strlen(a, b));
//	return 0;
//}

//输入一个整数数组，实现一个函数，
//来调整该数组中数字的顺序使得数组中所有的奇数位于数组的前半部分，
//所有偶数位于数组的后半部分。
#include<stdio.h>
int* my_adjust(int* a, int sz)
{
	int i = 0, j = 0;
	int tmp = 0;
	for (i = 0; i < sz; i++)
	{
		//奇数往前放
		if (a[i] % 2 == 1)
		{
			tmp = a[j];
			a[j] = a[i];
			a[i] = tmp;
			j++;
		}
	}
}
void print(int* a, int sz)
{
	int i = 0;
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}
int main()
{
	int a[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	int sz = sizeof(a) / sizeof(a[0]);
	print(a, sz);
	my_adjust(a, sz);
	print(a, sz);
	return 0;
}