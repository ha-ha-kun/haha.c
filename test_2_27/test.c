#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<assert.h>
//void mystrcpy(char* dest, char* src)
//{
//	while (*src != '\0')
//	{
//		//拷贝
//		*dest = *src;
//		//往后指向下一个位置
//		dest++;
//		src++;
//	}
//	*dest = *src;//拷贝结束标志'\0'
//}

//void mystrcpy(char* dest, char* src)
//{
//	// 后置++实现指针往后推进，
//	// 赋值操作=返回左操作值，达到while循环结束判断标志的同时，
//	// 对结束标志'\0'进行拷贝
//	while (*dest++ = *src++);
//}


// const
//		在*左边，表示修饰 *p，也就是指针指向的内容，此时不可更改指针p指向
//			的地址中的值，但指针可以指向别的位置；
//		在*右边，表示修饰  p，只修饰指针，此时p只可指向当前所指向的地址，
//			而其中的值，可被更改。
//
//void mystrcpy(char* dest, const char* src)
//{
//	assert(dest && src);//防止dest与src为空指针
//	while (*dest++ = *src++);
//}

//char* mystrcpy(char* dest, const char* src)
//{
//	assert(dest && src);//防止dest与src为空指针
//	char* ret = dest;//保存源字符串首地址
//	while (*dest++ = *src++);
//	return ret;
//}

char* mystrcpy(char* dest, const char* src, int dsz)
{
	int i = 0;//用于测算源字符串长度是否满足条件
	assert(dest && src);//防止dest与src为空指针
	char* ret = dest;//保存源字符串首地址
	while (*dest++ = *src++)
	{
		i++;
		assert(i < dsz);
	}
	return ret;
}


int main()
{
	char* p = NULL;
	char arr1[] = {"hello world"};
	char arr2[12] = {0};
	int sz = sizeof(arr2) / sizeof(arr2[0]);
	//mystrcpy(p, arr1);
	//mystrcpy(arr2, arr1);
	mystrcpy(arr2, arr1, sz);
	printf("%s\n", arr2);
	return 0;
}