#define _CRT_SECURE_NO_WARNINGS 1

//#include<stdio.h>
////不创建变量完成两个变量的数值交换
//// 原理：归零律：a ^ a = 0
////       恒等律：a ^ 0 = a
////		 交换律：a ^ b = b ^ a
////		 结合律：a ^ b ^ c =(a ^ b) ^ c = a ^ (b ^ c)
////		 
////
//int main()
//{
//	int a = 1;
//	int b = 2;
//	a = a ^ b;
//	b = a ^ b;//b = (a ^ b) ^ b = a ^ b ^ b = a ^ (b ^ b) = a ^ 0 = a;
//	a = a ^ b;//a = a ^ b ^ a = b ^ 0;
//	printf("a = %d, b = %d\n", a, b);
//	return 0;
//}

////求一个整数存储在内存中的二进制中1的个数
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	int count = 0;
//	for (int i = 0; i < 32; i++)
//	{
//		if (1 == ((a >> i) & 1))
//		{
//			count++;
//		}
//	}
//	printf("%d", count);
//	return 0;
//}

#include<stdio.h>
int main()
{
	int a = 0;
	scanf("%d", &a);
	a = a > 0 ? 1 : 0;//a大于0，a = 1； 否则 a = 0；
	printf("%d", a);
}