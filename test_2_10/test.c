#define _CRT_SECURE_NO_WARNINGS 1

////利用函数输出100到200之间的素数
//#include<stdio.h>
//#include<stdbool.h>
//bool judgeprime(int a)
//{
//	int i = 0;
//	for (i = a - 1; i > 1; i--)
//	{
//		if (a % i == 0)
//			return false;
//	}
//	return true;
//}
//int main()
//{
//	int x = 0;
//	for (x = 100; x < 201; x++)
//	{
//		if (judgeprime(x))
//			printf("%d是素数\n", x);
//		else
//			printf("%d不是素数\n", x);
//	}
//	return 0;
//}

////利用函数判断是否为闰年
//#include<stdio.h>
//#include<stdbool.h>
//bool judgeleap(int a)
//{
//	if (a % 4 == 0 && a % 100 != 0)
//		return true;
//	if (a % 400 == 0)
//		return true;
//	return false;
//}
//int main()
//{
//	int x = 0;
//	while (scanf("%d", &x) != EOF)
//	{
//		if (judgeleap(x))
//			printf("%d年是闰年\n", x);
//		else
//			printf("%d年不是闰年\n", x);
//	}
//	return 0;
//}

////利用函数交换两个整数的内容
//#include<stdio.h>
//void swap(int* a, int* b)
//{
//	int temp = 0;
//	temp = *a;
//	*a = *b;
//	*b = temp;
//}
//int main()
//{
//	int a = 0, b = 0;
//	scanf("%d%d", &a, &b);
//	swap(&a, &b);
//	printf("交换后为：a = %d, b = %d", a, b);
//	return 0;
//}

//利用函数输出乘法口诀表，大小自己指定
#include<stdio.h>
void printtable(int a)
{
	int x = 0, y = 0;
	for (x = 1; x <= a; x++)
	{
		for (y = 1; y <= x; y++)
			printf("%d * %d = %d ", x, y, x * y);
		printf("\n");
	}
}
int main()
{
	int a = 0;
	printf("请输入要打印的表大小：\n");
	scanf("%d", &a);
	printtable(a);
}