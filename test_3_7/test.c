#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
////模仿qsort的功能实现一个通用的冒泡排序
////交换
//void swap(char* a, char* b, int w)
//{
//	char tmp = 0;
//	for (int i = 0; i < w; i++)
//	{
//		tmp = *a;
//		*a = *b;
//		*b = tmp;
//		a++;
//		b++;
//	}
//}
////自定义qsort函数
//my_qsort(void* base, int num, int width, int (*cmp)(const void* e1, const void* e2))
//{
//	//n - 1趟比较
//	for (int i = 0; i < num - 1; i++)
//	{
//		//比较
//		for (int j = 1; j < num; j++)
//		{
//			//交换
//			if (cmp(((char*)base + (j - 1) * width), ((char*)base + j * width)) > 0)
//			{
//				swap(((char*)base + (j - 1) * width), ((char*)base + j * width), width);
//			}
//			
//		}
//	}
//}
////自定义打印函数
//void print(void* a, int sz, int width)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		switch (width)
//		{
//		case 1:
//			printf("%c ", *((char*)a + i));
//			break;
//		case 4:
//			printf("%d ", *((int*)a + i));
//			break;
//		case 8:
//			printf("%f ", *((float*)a + i));
//			break;
//		default:
//			break;
//		}
//	}
//	printf("\n");
//}
////整型比较函数
//int cmp_int(const void* e1, const void* e2)
//{
//	return *((int*)e1) - *((int*)e2);
//}
////字符型比较函数
//int cmp_char(const void* e1, const void* e2)
//{
//	return *((char*)e1) - *((char*)e2);
//}
//int main()
//{
//	int arr[] = { 10,9,8,7,6,5,4,3,2,1 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	my_qsort(arr, sz, sizeof(arr[0]), cmp_int);
//	//char arr[] = { "fedcba" };
//	//int sz = sizeof(arr) / sizeof(arr[0]) - 1;
//	//my_qsort(arr, sz, sizeof(arr[0]), cmp_char);
//	print(arr, sz, sizeof(arr[0]));
//	return 0;
//}
////整型比较函数
//int cmp_int(const void* e1, const void* e2)
//{
//	return *((int*)e1) - *((int*)e2);
//}
////字符型比较函数
//int cmp_char(const void* e1, const void* e2)
//{
//	return *((char*)e1) - *((char*)e2);
//}
//void print(void* a, int sz, int width)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		switch (width)
//		{
//		case 1:
//			printf("%c ", *((char*)a + i));
//			break;
//		case 4:
//			printf("%d ", *((int*)a + i));
//			break;
//		case 8:
//			printf("%f ", *((float*)a + i));
//			break;
//		default:
//			break;
//		}
//	}
//	printf("\n");
//}
//int main()
//{
//	int arr1[] = { 10,9,8,7,6,5,4,3,2,1 };
//	int sz1 = sizeof(arr1) / sizeof(arr1[0]);
//	qsort(arr1, sz1, sizeof(arr1[0]), cmp_int);
//	char arr2[] = { "fedcba" };
//	int sz2 = sizeof(arr2) / sizeof(arr2[0]) - 1;
//	qsort(arr2, sz2, sizeof(arr2[0]), cmp_char);
//	print(arr1, sz1, sizeof(arr1[0]));
//	print(arr2, sz2, sizeof(arr2[0]));
//}

//写一个函数，判断一个字符串是否为另外一个字符串旋转之后的字符串。
//旋转函数
void revolve(char* dest, const char* src, int sz, int n)
{
	for (int i = 0; i < sz - 1; i++)
	{
		*(dest + i) = *(src + (i +  n) % (sz - 1));//左转：src后面的字符往前移n个，前面的放到后面
	}
	*(dest + sz - 1) = '\0';
}
//判断函数
int judge_revolve(const char* a, const char* b, int sz)
{
	int ret = 0;
	//暂存字符串
	char tmp[100] = { 0 };
	//比较,检查字符是否相同
	for (int i = 0; i < sz - 1; i++)
	{
		revolve(tmp, a, sz, i);
		if (strcmp(tmp, b) == 0)
		{
			//字符相同跳出循环
			ret = i;
			break;
		}
	}
	return ret;
}

void output(char* a, char* b, int sz, int n)
{
	if (strcmp(a, b) == 0)
	{
		printf("两字符串相同！");
	}
	else if (n == 0)
	{
		printf("不可以通过旋转得到");
	}
	else if (n <= (sz - 1) / 2)
	{
		printf("左转%d个\n", n);
	}
	else
	{
		printf("右转%d个\n", sz - n - 1);
	}
}
int main()
{
	char a[] = { "abcdef" };
	char b[] = { "cdefab" };
	char c[] = { "fabcde" };
	char d[] = { "abcdef" };
	char e[] = { "acdaba" };
	int sz = sizeof(a) / sizeof(a[0]);
	int n = judge_revolve(a, e, sz);
	output(a, e, sz, n);
}