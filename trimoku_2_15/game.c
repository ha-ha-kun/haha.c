#define _CRT_SECURE_NO_WARNINGS 1

//游戏相关函数的实现
#include"game.h"

//游戏菜单
void menu()
{
	printf("**********************\n");
	printf("****** 1. play *******\n");
	printf("****** 0. exit *******\n");
	printf("**********************\n");
}

//游戏棋子
void Initboard(char board[Row][Column], int row, int column)
{
	int i = 0;
	for (i = 0; i <= row - 1; i++)
	{
		int j = 0;
		for (j = 0; j <= column - 1; j++)
		{
			board[i][j] = ' ';
		}
	}
}

//空棋盘
void Display(char board[Row][Column], int row, int column)
{
	int i = 0;
	for (i = 0; i <= row - 1; i++)
	{
		int j = 0;
		for (j = 0; j <= column - 1; j++)
		{
			printf(" %c ", board[i][j]);
			if (j < column - 1)
			{
				printf("|");
			}
		}
		printf("\n");
		if (i < row - 1)
		{
			int j = 0;
			for (j = 1; j <= 4 * column - 1; j++)
			{
				printf("-");
			}
			printf("\n");
		}
	}

}

//下棋
void Chess(char board[Row][Column],int row, int column)
{
	int m = 0;
	int n = 0;
	printf("玩家走:");
	while (1)
	{
		printf("请输入坐标;");
		scanf("%d%d", &m, &n);
		if (m < 1 || m > row || n < 1 || n > column)
		{
			printf("坐标错误，请重新输入!\n");
		}
		else if (board[m - 1][n - 1] != ' ')
		{
			printf("该位置已被占用，请重新输入!\n");
		}
		else 
		{
			board[m - 1][n - 1] = '*';
		break;
		}
		
	}
	
}

void ComputerMove(char board[Row][Column], int row, int column)
{
	printf("电脑走:\n");

	while (1)
	{
		int m = rand() % row;
		int n = rand() % column;
		if (board[m][n] == ' ')
		{
			board[m][n] = '#';
			break;
		}
	}
}

int IsFull(char board[Row][Column], int row, int column)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < column; j++)
		{
			if (board[i][j] == ' ')
			{
				return 0;
			}
		}
	}
	return 1;
}

char Iswin(char board[Row][Column], int row, int column)
{
	int a = 0;
	for (a = 0; a < row; a++)
	{
		if (board[a][0] == board[a][1] && board[a][1] == board[a][2] && board[a][1] != ' ')
		{
			return board[a][0];
		}
	}
	for (a = 0; a < row; a++)
	{
		if (board[0][a] == board[1][a] && board[1][a] == board[2][a] && board[1][a] != ' ')
		{
			return board[0][a];
		}
	}
	if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[1][1] != ' ')
	{
		return board[1][1];
	}
	if (board[2][0] == board[1][1] && board[1][1] == board[0][2] && board[1][1] != ' ')
	{
		return board[1][1];
	}
	int ret = IsFull(board, row, column);
	if (ret == 0)
	{
		return 'C';
	}
	else
	{
		return 'Q';
	}
 }