#pragma once
//关于游戏相关的函数声明，符号声明；头文件的包含
//头文件的包含
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
//棋盘大小
#define Row 3
#define Column 3

//初始化棋盘
void Initboard(char board[Row][Column], int row, int column);

//打印棋盘
void Display(char board[Row][Column], int row, int column);

void Chess(char board[Row][Column], int row, int column);

void ComputerMove(char board[Row][Column], int row, int column);

int IsFull(char board[Row][Column], int row, int column);

char Iswin(char board[Row][Column], int row, int column);