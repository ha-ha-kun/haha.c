#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

//int main()
//{
//	FILE* pf;//定义文件类型指针
//	pf = fopen("test.txt", "r");//以"r"模式打开文件pf
//	//判断是否打开成功
//	if (pf == NULL)
//	{
//		perror("fopen::");
//		return 1;
//	}
//;
//	char a[6] = { "abcdef" };
//	int b = fputs(a, pf);
//	printf("%s\n", a);
//	printf("%d\n", b);
//	
//	return 0;
//}


//FILE* pf;//定义文件类型指针
//pf = fopen("test.txt", "r");//以"r"模式打开文件pf
////判断是否打开成功
//if (pf == NULL)
//{
//	perror("fopen::");
//	return 1;
//}
	/*int x = fgetc(stdin);
	int y = fgetc(stdin);
	int z = fgetc(stdin);
	printf("%c\n", x);
	printf("%c\n", y);
	printf("%c\n", z);*/

//验证顺序读写函数
//int main()
//{
//	FILE* pf;//定义文件类型指针
//	pf = fopen("test.txt", "r");//以"r"模式打开文件pf
//	//判断是否打开成功
//	if (pf == NULL)
//	{
//		perror("fopen::");
//		return 1;
//	}
//	struct stu
//	{
//		char name[20];
//		int age;
//		double score;
//	};
//	struct stu a = { "zhangsan", 20, 80.0};
//	int b = 0;
//	b = fprintf(pf, "%s %d %lf", a.name, a.age, a.score);
//	//b = fscanf(pf, "%s %d %f", a.name, &(a.name), &(a.score));
//	printf("%s %d %f\n", a.name, a.age, a.score);
//	printf("b = %d", b);
//	return 0;
//}

int main()
{
	FILE* pf;//定义文件类型指针
	pf = fopen("test.txt", "r");//以"r"模式打开文件pf
	//判断是否打开成功
	if (pf == NULL)
	{
		perror("fopen::");
		return 1;
	}
	fseek(pf, 1, SEEK_CUR);
	int x = fgetc(pf);
	printf("%c\n", x);
	fgetc(pf);
	x = fgetc(pf);
	printf("%c\n", x);
	rewind(pf);
	fseek(pf, 2, SEEK_CUR);
	x = fgetc(pf);
	printf("%c\n", x);

	return 0;
}