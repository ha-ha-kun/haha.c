#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<malloc.h>

// 猴子吃桃问题
// 重点：提炼问题中的数学模型
// 假设吃了两天，第二天只剩a个， 问题便是 (a + 1) * 2 便是第一天摘的果子
// 

// 返回第n天时的果子数量
int nPeach(int n)
{
	if (n == 1)
	{
		return 1;
	}
	else
	{
		return 2 * (nPeach(n - 1) + 1);
	}
}
//
//int main()
//{
//	int n = 10;
//	printf("%d", nPeach(n));
//	return 0;
//}

// 消失的数字
// 1、用一个额外的数组 malloc(n+1)
int Findnum1(int* a, int sz)
{
	int* p = (int*)malloc((sz + 1) * sizeof(int));
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		p[a[i]] = a[i];
	}
	for (i = 0; i < sz + 1; i++)
	{
		if (p[i] != i)
			return i;
	}
}

// 2、异或，用0与所有的数据异或，然后再与0~n之间的所有数异或, 0和相同的两个数异或，结果为0

int Findnum2(int* a, int sz)
{
	int x = 0;
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		x ^= a[i];
	}
	for (i = 0; i < sz + 1; i++)
	{
		x ^= i;
	}
	return x;
}

// 3、排序+二分查找 由于排序时间复杂度高，故实现功能较差
// 4、公式计算 求和公式相减，与数学结合，大大提高效率

int Findnum4(int* a, int sz)
{
	int i = 0;
	int sum = (1 + sz)  * sz / 2; // 相相乘再除，防止整数除法造成的精度丢失
	for (i = 0; i < sz; i++)
		sum -= a[i];
	return sum;
}

int main()
{
	int a[] = { 0,1,2,4 };
	printf("%d ", Findnum1(a, sizeof(a) / sizeof(a[0])));
	printf("%d ", Findnum2(a, sizeof(a) / sizeof(a[0])));
	printf("%d ", Findnum4(a, sizeof(a) / sizeof(a[0])));
	return 0;
}