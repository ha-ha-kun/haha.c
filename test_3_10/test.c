#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<assert.h>
#include<string.h>
//模拟实现strstr
//在str1中寻找str2，若存在则返回首次出现的地址，不存在返回NULL；
char* my_strstr(const char* str1, const char* str2)
{
	//断言：str1 和 str2 指向的地址不可为空
	assert(*str1 && *str2);
	const char* s1 = str1, * s2 = str2;//寻找相同的字符
	const char* cur = str1;//记录相同字符串出现时，str1中的首地址；
	//cur 不为'\0'则往后寻找
	while (*cur)
	{
		s1 = cur;
		s2 = str2;
		//s1,s2指向的地址不为空且相同则开始寻找
		while (*s1 && *s2 && *s1 == *s2)
		{
			s1++;
			s2++;
		}
		if (*s2 == '\0'	)
			return cur;
		cur++;
	}
	return NULL;
}

//模拟实现strcat
char* my_strcat(char* str1, const char* str2)
{
	//指针不为野指针
	assert(str1 && str2);
	//找到str1末尾
	char* s = str1;
	while (*++s);
	while (*s++ = *str2++);
	s = str2;
	return str1;
}

//模拟实现strcmp
int my_strcmp(const char* str1, const char* str2)
{
	assert(*str1 && *str2);
	while (*str1++ == *str2++);
	return *--str1 - *--str2;
}

//模拟实现strcpy
char* my_strcpy(char* str1, const char* str2)
{
	assert(str1 && str2);
	char* ret = str1;
	while (*str1++ = *str2++);
	*str1 = *str2;
	return ret;

}

//模拟实现strlen
size_t my_strlen(char* s)
{
	assert(s);
	int count = 0;
	while (*s++)
	{
		count++;
	}
	return count;
}
int main()
{
	char arr1[20] = { "abcdefgh" };
	char arr2[] = { "hello" };
	printf("%d\n", my_strlen(arr1));
}