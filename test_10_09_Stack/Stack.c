#define _CRT_SECURE_NO_WARNINGS 1
#include"Stack.h"

void Checkcapacity(Stack* ps)
{
	assert(ps);
	// 每次增加10个空间
	if (ps->capacity == ps->top)
	{
		int newcapacity = ps->capacity == 0 ? 4 : 2 * ps->capacity;
		STDataType* tmp = (STDataType*)realloc(ps->a, sizeof(STDataType) * newcapacity);
		if (tmp == NULL)
		{
			perror("malloc failed");
			exit(-1);
		}
		ps->a = tmp;
		ps->capacity = newcapacity;
	}
}

void StackInit(Stack* ps)
{
	assert(ps);
	ps->capacity = ps->top = 0;
	ps->a = NULL; // 指针需置空，否则在检查容量时使用，会出现访问野指针的情况
	Checkcapacity(ps);
}

void StackPush(Stack* ps, STDataType data)
{
	assert(ps);
	Checkcapacity(ps);

	ps->a[ps->top++] = data;
}

void StackPop(Stack* ps)
{
	assert(ps);
	// 有数据才可删
	assert(ps->top);

	ps->top--;
}

STDataType StackTop(Stack* ps)
{
	assert(ps);
	assert(ps->top);

	return ps->a[ps->top - 1];
}

int StackSize(Stack* ps)
{
	assert(ps);
	return ps->top;
}

int StackEmpty(Stack* ps)
{
	assert(ps);

	return ps->top == 0;
}

void StackDestroy(Stack* ps)
{
	free(ps->a);
	ps->capacity = ps->top = 0;
}