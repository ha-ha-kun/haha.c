#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

struct Node {
    int val;
    struct Node* next;
    struct Node* random;
    
};
struct Node* copyRandomList(struct Node* head) {//传值调用
    if (head == NULL) {
        return NULL;//空链表返回空
    }
    // 在原有的每个结点之后添加此节点的备份，使每个源节点的next指向其复制结点
    for (struct Node* node = head; node != NULL; node = node->next->next) {
        struct Node* nodeNew = malloc(sizeof(struct Node));
        nodeNew->val = node->val;
        nodeNew->next = node->next;
        node->next = nodeNew;
    }
    // 修改新增加节点中的random，使之指向对应的新节点
    for (struct Node* node = head; node != NULL; node = node->next->next) {
        struct Node* nodeNew = node->next;
        nodeNew->random = (node->random != NULL) ? node->random->next : NULL;
    }
    // 将链表拆开，新旧链表分别合并
    struct Node* headNew = head->next;
    for (struct Node* node = head; node != NULL; node = node->next) {
        struct Node* nodeNew = node->next;
        node->next = node->next->next;
        nodeNew->next = (nodeNew->next != NULL) ? nodeNew->next->next : NULL;
    }
    return headNew;
}

