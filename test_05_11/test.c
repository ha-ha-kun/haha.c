#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

struct ListNode {
	int val;
	struct ListNode* next;
};

typedef struct ListNode node;
// 删除链表中重复的元素
struct ListNode* deleteDuplication(struct ListNode* pHead) {
    node* head = (node*)malloc(sizeof(node));
    node* cur = head;
    cur->next = pHead;
    // 判断
    while (cur->next)
    {
        // 重复
        if (!cur->next->next)
        {
            break;
        }
        int tmp = 0;
        if (cur->next->val == cur->next->next->val)
        {
            tmp = cur->next->val;
            cur->next = cur->next->next; // 删除1个重复结点
            while (cur->next->val == tmp && cur->next)
            {
                // 删除多余的重复结点
                cur->next = cur->next->next;
            }
        }
        else {
            {
                cur = cur->next;
            }
        }
    }
    pHead = head->next;
    free(head);
    return pHead;
}

// 对链表进行插入排序
typedef struct ListNode node;
struct ListNode* insertionSortList(struct ListNode* head) {
    // 空链表直接返回
    if (!head)
    {
        return head;
    }
    node* tmp = (node*)malloc(sizeof(node));
    tmp->next = head;
    node* cur = head->next; // cur指向待排序链表
    node* last = tmp->next; // last指向链表中最大的元素
    while (cur)
    {
        if (last->val <= cur->val)
        {
            // 已有序，往下走
            last = last->next;
        }
        else
        {
            node* pre = tmp;
            while (pre->next->val <= cur->val)
            {
                pre = pre->next;
            }
            // 将cur指向的结点插入到pre后，last指向的结点接上下一个结点
            last->next = cur->next;
            cur->next = pre->next;
            pre->next = cur;
        }
        cur = last->next;
    }
    head = tmp->next;
    free(tmp);
    return head;
}
