#define _CRT_SECURE_NO_WARNINGS 1
#include<assert.h>

// 给你一个 升序排列 的数组 nums ，请你 原地 删除重复出现的元素，使每个元素 只出现一次 ，返回删除后数组的新长度。元素的 相对顺序 应该保持 一致 。
// 由于在某些语言中不能改变数组的长度，所以必须将结果放在数组nums的第一部分。更规范地说，如果在删除重复项之后有 k 个元素，那么 nums 的前 k 个元素应该保存最终结果。
// 将最终结果插入 nums 的前 k 个位置后返回 k 。
// 不要使用额外的空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。

int removeDuplicates(int* nums, int numsSize)
{
    //元素前移数量
    int count = 0;
    int i = 0;
    // 第一个数默认不重复
    for (i = 1; i < numsSize; i++)
    {
        //元素前移
        nums[i] = nums[i + count];
        //判断是否相等
        while (nums[i - 1] == nums[i])
        {
            //前移数+1
            count++;
            //返回大小-1
            numsSize--;
            //比较下一个值
            nums[i] = nums[i + count];
        }
    }
    return numsSize;
}

int main()
{
    int nums[] = { 0,0,1,1,1,2,2,3,3,4 }; // 输入数组
    int expectedNums[] = { 0,1,2,3,4 }; // 长度正确的期望答案

    int k = removeDuplicates(nums, sizeof(nums) / sizeof(nums[0])); // 调用

    for (int i = 0; i < k; i++) {
        assert (nums[i] == expectedNums[i]);
    }
    return 0;
}