#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
#include<string.h>
#include<time.h>

typedef int HPDataType;
typedef struct Heap
{
	HPDataType* a;
	int size;
	int capacity;
}HP;
// 向上调整
void AdjustUp(HPDataType* a, int child);
// 向下调整
void AdjustDown(HPDataType* a, int n, int parent);

// 交换
void Swap(HPDataType* p1, HPDataType* p2);
// 打印堆
void HeapPrint(HP* php);
// 初始化堆
void HeapInit(HP* php);
// 以a中内容建堆
void HeapInitArray(HP* php, int* a, int n);
// 销毁堆
void HeapDestroy(HP* php);
// 插入数据
void HeapPush(HP* php, HPDataType x);
// 删除数据
void HeapPop(HP* php);
// 访问堆顶
HPDataType HeapTop(HP* php);
// 判空
bool HeapEmpty(HP* php);
