#define _CRT_SECURE_NO_WARNINGS 1
#include"Heap.h"

void CreateNDate()
{
	// 造数据
	int n = 10000;
	srand(time(0));

	const char* file = "data.txt";
	FILE* fin = fopen(file, "w");
	if (fin == NULL)
	{
		perror("fopen error");
		exit(-1);
	}
	for (size_t i = 0; i < n; i++)
	{
		int x = rand() % 1000000;
		fprintf(fin, "%d\n", x);
	}

	fclose(fin);
}

void PrintTopK(const char* filename, int k)
{
	// 建堆
	FILE* fout = fopen(filename, "r");
	if (fout == NULL)
	{
		printf("fopen failed");
		exit(-1);
	}
	int* minheap = (int*)malloc(sizeof(int) * k);
	if (minheap == NULL)
	{
		printf("malloc failed");
		exit(-1);
	}
	// 将前k个数输入到堆中
	for (size_t i = 0; i < k; i++)
	{
		fscanf(fout, "%d", &minheap[i]);
	}
	// 调整为小堆,完全二叉树中若有n个节点，则其最后一个节点的序号为(k-2)/2，
	for (int i = (k - 2) / 2; i >= 0; i--)
	{
		AdjustDown(minheap, k, i);
	}

	// 将剩下的元素依次与堆顶元素比较，大则入堆
	int x = 0;
	while (fscanf(fout, "%d", &x) != EOF)
	{
		if (x > minheap[0])
		{
			// 入堆
			minheap[0] = x;
			AdjustDown(minheap, k, 0);
		}
	}
	for (size_t i = 0; i < k; i++)
	{
		printf("%d ", minheap[i]);
	}
	printf("\n");

	free(minheap);
	fclose(fout);

}

void Heapsort(int* a, int n)
{
	// 降序，考虑建小堆，然后依次执行类似pop操作，将堆顶置于数组尾部
	// 向下调整建堆
	for (int i = (n - 2) / 2; i >= 0; i--)
	{
		AdjustDown(a, n, i);
	}
	int end = n - 1;
	// 排序
	while (end > 0)
	{
		Swap(&a[0], &a[end]);
		AdjustDown(a, end, 0);
		end--;
	}
}


void Test1()
{
	int a[] = { 11,2,43,7,8,5,73,6,10,9,3 };
	HP* h = (HP*)malloc(sizeof(HP));
	HeapInit(h);
	HeapInitArray(h, a, sizeof(a) / sizeof(int));

	HeapPrint(h);

	HeapPush(h, 1);
	HeapPush(h, 100);
	HeapPush(h, 4);
	HeapPush(h, 99);

	HeapPrint(h);


	HeapPop(h);
	HeapPop(h);
	HeapPop(h);
	HeapPrint(h);

	printf("%d\n", HeapTop(h));
	HeapPop(h);

	HeapPrint(h);


	HeapDestroy(h);
}

void Test2()
{
	//CreateNDate();
	PrintTopK("data.txt", 10);
}

void Test3()
{
	int a[] = { 11,2,43,7,8,5,73,6,10,9,3 };
	Heapsort(a, sizeof(a) / sizeof(a[0]));
}

int main()
{
	Test2();
	return 0;
}