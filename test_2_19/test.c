#define _CRT_SECURE_NO_WARNINGS 1


////两个int（32位）整数m和n的二进制表达中，有多少个位(bit)不同？ 
//#include<stdio.h>
//int main()
//{
//	int m = 0, n = 0;
//	int count = 0;//用于存储不同的二进制数目
//	scanf("%d %d", &m, &n);
//	for (int i = 0; i < 32; i++)
//	{
//		if (((m >> i) & 1) != ((n >> i) & 1))
//		{
//			count++;
//		}
//	}
//	printf("%d", count);
//	return 0;
//}

////获取一个整数二进制序列中所有的偶数位和奇数位，分别打印出二进制序列
//#include<stdio.h>
//int main()
//{
//	//a数组用于存储奇数位，b存储偶数位
//	int a[16] = { 0 };
//	int b[16] = { 0 };
//	int x = 0;
//	scanf("%d", &x);
//	//分离奇数位与偶数位
//	for (int i = 1, j = 0; i < 32; i += 2, j++)
//	{
//		a[j] = ((x >> i) & 1);
//	}
//	for (int i = 0, j = 0; i < 32; i += 2, j++)
//	{
//		b[j] = ((x >> i) & 1);
//	}
//	//打印
//	printf("奇数位：\n");
//	for (int i = 15; i >= 0; i--)
//	{
//		printf("%d", a[i]);
//	}
//	printf("\n偶数位：\n");
//	for (int i = 15; i >= 0; i--)
//	{
//		printf("%d", b[i]);
//	}
//	return 0;
//}

////写一个函数返回参数二进制中 1 的个数。
//#include<stdio.h>
//int retbin1(int a)
//{
//	int count = 0;//统计1的个数
//	for (int i = 0; i < 32; i++)
//	{
//		if (((a >> i) & 1) == 1)
//		{
//			count++;
//		}
//	}
//	return count;
//}
//int main()
//{
//	int x = 0;
//	scanf("%d", &x);
//	printf("%d",retbin1(x));
//	return 0;
//}

//交换两个变量（不创建临时变量）
#include<stdio.h>
int main()
{
	int x = 0, y = 0;
	scanf("%d %d", &x, &y);
	printf("交换前：x = %d, y = % d\n", x, y);
	x = x ^ y;
	y = x ^ y;
	x = x ^ y;
	printf("交换后：x = %d, y = % d\n", x, y);
	return 0;
}