#define _CRT_SECURE_NO_WARNINGS 1

// 给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。
// 不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地 修改输入数组。
// 元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。

// ==val的元素后移便可
int removeElement(int* nums, int numsSize, int val) {
	int count = 0;
	int i = 0;
	for (i = 0; i < numsSize; i++)
	{
		//前移
		nums[i] = nums[count + i];
		while (nums[i] == val)
		{
			//确定前移个数
			count++;
			numsSize--;
			//前移
			nums[i] = nums[count + i];
		}
	}
	return numsSize;
}

int main()
{
	int a[] = { 3,2,2,3 };
	int n = sizeof(a) / sizeof(a[0]);
	int x = removeElement(a, n, 3);
	
	return 0;
}