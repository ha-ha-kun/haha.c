#define _CRT_SECURE_NO_WARNINGS 1
#include"List.h"

TestDlist()
{
	ListNode* L = ListCreate();
	ListPushBack(L, 1);
	ListPushBack(L, 2);
	ListPushBack(L, 3);
	ListPushBack(L, 4);
	ListPushBack(L, 5);

	ListPrint(L);

	ListPushFront(L, 6);
	ListPushFront(L, 7);
	ListPushFront(L, 8);
	ListPushFront(L, 9);
	ListPushFront(L, 0);

	ListPrint(L);

	ListPopBack(L);
	ListPopBack(L);
	ListPopBack(L);

	ListPrint(L);

	ListPopFront(L);
	ListPopFront(L);
	ListPopFront(L);

	ListPrint(L);

	printf("%p\n", ListFind(L, 1));

	ListErase(ListFind(L, 2));
	ListPrint(L);

}

int main()
{
	TestDlist();
}