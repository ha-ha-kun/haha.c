#define _CRT_SECURE_NO_WARNINGS 1
#include"List.h"

ListNode* BuyLNode(LTDataType x)
{
	ListNode* node = (ListNode*)malloc(sizeof(ListNode));
	if (!node)
	{
		perror("malloc failed");
		exit(-1);
	}
	node->data = x;
	node->next = NULL;
	node->prev = NULL;

	return node;
}

ListNode* ListCreate()
{
	ListNode* head = BuyLNode(0);
	head->next = head;
	head->prev = head;
	
	return head;
}

void ListDestory(ListNode* pHead)
{
	assert(pHead);
	
	// 销毁其余结点
	ListNode* cur = pHead->next, *pre = NULL;
	while (cur != pHead)
	{
		pre = cur;
		cur = cur->next;
		free(pre);
	}
	// 销毁头节点
	free(pHead);
}

void ListPrint(ListNode* pHead)
{
	// 判空
	assert(pHead);
	// 需要存在结点
	assert(pHead->next != pHead);

	ListNode* cur = pHead->next;
	while (cur != pHead)
	{
		printf("%d ", cur->data);
		cur = cur->next;
	}
	printf("\n");
}

void ListPushBack(ListNode* pHead, LTDataType x)
{
	assert(pHead);

	ListNode* new = BuyLNode(x);
	new->prev = pHead->prev;
	pHead->prev->next = new;
	pHead->prev = new;
	new->next = pHead;
}

void ListPopBack(ListNode* pHead)
{
	assert(pHead);
	assert(pHead->next != pHead);


	ListNode* del = pHead->prev;
	del->prev->next = pHead;
	pHead->prev = del->prev;
	free(del);
}

void ListPushFront(ListNode* pHead, LTDataType x)
{
	assert(pHead);

	ListNode* new = BuyLNode(x);
	new->next = pHead->next;
	pHead->next->prev = new;
	pHead->next = new;
	new->prev = pHead;

}

void ListPopFront(ListNode* pHead)
{
	assert(pHead);
	assert(pHead->next != pHead);

	ListNode* del = pHead->next;
	del->next->prev = pHead;
	pHead->next = del->next;

	free(del);
}

ListNode* ListFind(ListNode* pHead, LTDataType x)
{
	assert(pHead);
	assert(pHead->next != pHead);

	ListNode* cur = pHead->next;
	while (cur != pHead)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next;
	}
	return NULL;
}

void ListInsert(ListNode* pos, LTDataType x)
{
	assert(pos);
	
	ListNode* new = BuyLNode(x);

	pos->prev->next = new;
	new->prev = pos->prev;
	pos->prev = new;
	new->next = pos;
}

void ListErase(ListNode* pos)
{
	assert(pos);

	pos->prev->next = pos->next;
	pos->next->prev = pos->prev;
	free(pos);
}