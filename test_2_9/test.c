#define _CRT_SECURE_NO_WARNINGS 1

////输入两个随机数，输出二者最大公约数
//#include<stdio.h>
//int main()
//{
//	int a = 0, b = 0, i = 0;
//	scanf("%d%d", &a, &b);
//	for (i = a > b ? b : a; i > 0; i--)
//	{
//		if (a % i == 0 && b % i == 0)
//			printf("%d\n", i);
//		break;
//
//	}
//	return 0;
//}

////在1到100之间数9的个数
//#include<stdio.h>
//int main()
//{
//	int count = 0, i = 0;
//	int a = 0, b = 0;
//	for (i = 1; i < 101; i++)
//	{
//		//数个位
//		a = i % 10;
//		if (a % 9 == 0 && a != 0) count++;
//		//数十位
//		b = i / 10;
//		if (b % 9 == 0 && b != 0) count++;
//	}
//	printf("%d\n", count);
//	return 0;
//}

////输出乘法口诀表
//#include<stdio.h>
//int main()
//{
//	int x = 0, y = 0;
//	for (x = 1; x < 10; x++)
//	{
//		for (y = 1; y <= x; y++)
//		{
//			printf("%d * %d = %d ", x, y, x * y);
//		}
//		printf("\n");
//	}
//		
//	return	0;
//}

////猜数字游戏
//#include<stdio.h>
//int main()
//{
//	int x = 0, y = 0;
//	printf("请输入要猜的数：\n");
//	scanf("%d", &x);
//	printf("请猜数字：\n");
//	while (scanf("%d", &y))
//	{
//		if (y > x)
//			printf("猜大了\n");
//		else if (y < x)
//			printf("猜小了\n");
//		else
//		{
//			printf("恭喜你，猜对了\n");
//			break;
//		}
//	}
//	return 0;
//}

//二分查找，在一个有序数组中查找某个数，有则返回下标，无则告知没有
#include<stdio.h>
int main()
{
	int arr[10] = { 0 };
	int i = 0, x = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	int a = 0, b = sz - 1;
	int mid = (a+ b) / 2;
	//数组赋值
	for (i = 0; i < 10; i++)
	{
		arr[i] = 2 * (i + 1);
	}
	printf("请输入要查找的值：\n");
	scanf("%d", &x);
	while (a < b)
	{
		if (arr[a] > x || arr[b] < x)
			break;
		else if (arr[mid] < x)
				a = mid + 1;
		else
			b = mid - 1;
		mid = (a + b) / 2;

	}
	if (arr[a] == x)
		printf("该数的下标是：%d\n", a);
	else
		printf("找不到\n");
	return 0;
}