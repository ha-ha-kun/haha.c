#define _CRT_SECURE_NO_WARNINGS 

#include<stdio.h>
void exchange (int* x, int* y)
{
	int temp = 0;
	if (x < y)
	{
		temp = x;
		x = y;
		y = x;
	}
}
void swap(int* x, int* y)
{
	int temp = 0;
	temp = *x;
	*x = *y;
	*y = temp;
}
//随意三个数，从大到小输出
//int main()
//{
//	int a, b, c, temp;
//	scanf("%d%d%d", &a, &b, &c);
//	if (a < b)
//		swap(&a, &b);
//	if (a < c)
//		swap(&a, &c);
//	if (b < c)
//		swap(&b, &c);
//	printf("%d %d %d", a, b, c);
//	return 0;
//}
// 输入1-100之间3的倍数
//int main()
//{
//	int i;
//	for (i = 1; i < 101; i++)
//		if (i % 3 == 0)
//			printf("%d ", i);
//}
//输入随机10个数，输出最大值
//int main()
//{
//	int a[10] = { 0 };
//	int i = 0;
//	
//	for (i = 0; i < 10; i++)
//	{
//		scanf("%d", &a[i]);
//		
//	}
//	for (i = 1; i < 10; i++)
//	{
//		if (a[0] < a[i])
//			a[0] = a[i];
//	}
//	printf("%d", a[0]);
//	return 0;
//}
//输出100-200之间的素数/质数
//int main()
//{
//	int i = 0, j = 0;
//	for (i = 100; i < 200; i++)
//	{
//		int x = 0;
//		for (j = 2; j < i; j++)
//		{
//			if (i % j == 0)
//				x++;
//		}
//		if (x == 0)
//			printf("%d\n", i);
//	}
//	return 0;
//}
//int main()
//{
//	int x = 0;
//	for (x = 1000; x <= 2000; x++)
//	{
//		if (x % 4 == 0 && x % 100 != 0)
//			printf("%d\n", x);
//		if (x % 400 == 0)
//			printf("%d\n", x);
//	}
//}
//计算1/1-1/2+1/3-1/4+1/5 …… + 1/99 - 1/100 的值，打印出结果
int main()
{
	int i = 0;
	float sum = 0;
	for (i = 1; i < 101; i++)
	{
		if (i % 2 == 0)
			sum -= 1.0 / i;
		else
			sum += 1.0 / i;
	}
	printf("%f", sum);
	return 0;
}