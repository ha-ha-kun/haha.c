#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdbool.h>
#include<stdlib.h>

typedef struct {
    int* data;
    int head;
    int rear;
    int sz;
} MyCircularQueue;

MyCircularQueue* myCircularQueueCreate(int k) {
    MyCircularQueue* obj = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));
    // 多申请一个空间，便于判断队列是空还是满
    obj->data = (int*)malloc(sizeof(int) * (k + 1));
    obj->sz = k + 1;
    obj->head = obj->rear = 0;
    return obj;
}

bool myCircularQueueEnQueue(MyCircularQueue* obj, int value) {
    int tmp = (obj->rear + 1) % obj->sz;
    // 队列已满
    if (tmp == obj->head)
    {
        return false;
    }
    obj->data[obj->rear] = value;
    obj->rear = tmp;
    return true;
}

bool myCircularQueueDeQueue(MyCircularQueue* obj) {
    // 队列为空
    if (obj->head == obj->rear)
    {
        return false;
    }
    obj->head = (obj->head + 1) % obj->sz;
    return true;
}

int myCircularQueueFront(MyCircularQueue* obj) {
    if (obj->head == obj->rear)
    {
        return -1;
    }
    return obj->data[obj->head];
}

int myCircularQueueRear(MyCircularQueue* obj) {
    if (obj->head == obj->rear)
    {
        return -1;
    }
    // 此处需加大小，以防出现负数
    int tmp = (obj->rear - 1 + obj->sz) % obj->sz;
    return obj->data[tmp];
}

bool myCircularQueueIsEmpty(MyCircularQueue* obj) {
    return obj->head == obj->rear;
}

bool myCircularQueueIsFull(MyCircularQueue* obj) {
    int tmp = (obj->rear + 1) % obj->sz;
    return tmp == obj->head;
}

void myCircularQueueFree(MyCircularQueue* obj) {
    free(obj->data);
    obj->data = NULL;
    free(obj);
    obj = NULL;
}

int main()
{
    MyCircularQueue* Q = myCircularQueueCreate(20);
    myCircularQueueEnQueue(Q, 1);
    myCircularQueueEnQueue(Q, 2);
    myCircularQueueEnQueue(Q, 3);
    myCircularQueueEnQueue(Q, 4);

    return 0;
}