#define _CRT_SECURE_NO_WARNINGS 1
#include"SeqList.h"

void test1()
{
	Seqlist s;
	SLInit(&s);
	SLPushBack(&s, 1);
	SLprint(&s);
	SLPushBack(&s, 2);
	SLprint(&s);
	SLPushBack(&s, 3);
	SLprint(&s);
	SLPushBack(&s, 4);
	SLprint(&s);
	SLPushBack(&s, 5);
	SLprint(&s);
	SLPushBack(&s, 6);
	SLprint(&s);

}

void test2()
{
	Seqlist s;
	SLInit(&s);
	//SLPopBack(&s);
	SLPushBack(&s, 1);
	SLprint(&s);
	SLPushBack(&s, 2);
	SLprint(&s);
	SLPopBack(&s);
	SLPushBack(&s, 3);
	SLprint(&s);
	SLPushBack(&s, 4);
	SLprint(&s);
	SLPushBack(&s, 5);
	SLprint(&s);
	SLPopBack(&s);
	SLPushBack(&s, 6);
	SLprint(&s);

}
void test3()
{
	Seqlist s;
	SLInit(&s);
	//SLPopBack(&s);
	SLPushBack(&s, 1);
	SLprint(&s);
	SLPushBack(&s, 2);
	SLprint(&s);
	SLPushBack(&s, 3);
	SLprint(&s);
	SLPushBack(&s, 4);
	SLprint(&s);
	SLPushBack(&s, 5);
	SLprint(&s);
	SLPushBack(&s, 6);
	SLprint(&s);
	SLPushFront(&s, 1);
	SLPushFront(&s, 2);
	SLPushFront(&s, 3);
	SLprint(&s);
}
void test4()
{
	Seqlist s;
	SLInit(&s);
	//SLPopBack(&s);
	SLPushBack(&s, 1);
	SLprint(&s);
	SLPushBack(&s, 2);
	SLprint(&s);
	SLPushBack(&s, 3);
	SLprint(&s);
	SLPushBack(&s, 4);
	SLprint(&s);
	SLPushBack(&s, 5);
	SLprint(&s);
	SLPushBack(&s, 6);
	SLprint(&s);
	SLPushFront(&s, 1);
	SLPushFront(&s, 2);
	SLPushFront(&s, 3);
	SLprint(&s);
	SLPopFront(&s);
	SLprint(&s);
	SLPopFront(&s);
	SLprint(&s);
}
void test5()
{
	Seqlist s;
	SLInit(&s);
	//SLPopBack(&s);
	SLPushBack(&s, 1);
	SLprint(&s);
	SLPushBack(&s, 2);
	SLprint(&s);
	SLPushBack(&s, 3);
	SLprint(&s);
	SLPushBack(&s, 4);
	SLprint(&s);
	SLPushBack(&s, 5);
	SLprint(&s);
	SLPushBack(&s, 6);
	SLprint(&s);
	SLPushFront(&s, 1);
	SLPushFront(&s, 2);
	SLPushFront(&s, 3);
	SLprint(&s);
	SLInsert(&s, 1, 6);
	SLprint(&s);
	SLInsert(&s, 4, 6);
	SLprint(&s);
	SLInsert(&s, s.sz + 1, 88);
	SLprint(&s);
}

void test6()
{
	Seqlist s;
	SLInit(&s);
	//SLPopBack(&s);
	SLPushBack(&s, 1);
	SLPushBack(&s, 2);
	SLPushBack(&s, 3);
	SLPushBack(&s, 4);
	SLPushBack(&s, 5);
	SLPushBack(&s, 6);
	SLprint(&s);
	SLErase(&s, 1);
	SLprint(&s);
	SLErase(&s, s.sz);
	SLprint(&s);
}

void test7()
{
	Seqlist s;
	SLInit(&s);
	//SLPopBack(&s);
	SLPushBack(&s, 1);
	SLPushBack(&s, 2);
	SLPushBack(&s, 3);
	SLPushBack(&s, 4);
	SLPushBack(&s, 5);
	SLPushBack(&s, 6);
	SLprint(&s);
	int x = 0;
	do
	{
		scanf("%d", &x);
		printf("%d\n", SLFind(&s, x) + 1); // 第几个, 0表示没找到
	} while (x != 999);
}
int main()
{
	test7();
}