#define _CRT_SECURE_NO_WARNINGS 1
#include"SeqList.h"

// 初始化
void SLInit(Seqlist* s)
{
	s->data = NULL;
	s->sz = s->capacity = 0;
}

// 检查容量，空则给10个，满则翻倍
void SLcheck(Seqlist* s)
{
	if (s->sz == s->capacity)
	{
		// 初始给5个空间
		if (s->capacity == 0)
		{
			s->capacity = 1;
		}
		// 是否申请成功，每次扩容翻倍
		SLDATATYPE* tmp = (SLDATATYPE*)realloc(s->data, s->capacity * 2 * sizeof(SLDATATYPE));
		if (tmp != NULL)
		{
			s->data = tmp;
			s->capacity *= 2;
		}
		else
		{
			perror("SLcheck::realloc:");
			exit(1);
		}
		// 测试用
		printf("容量为：%d\n", s->capacity);
	}
}

// 尾插数据
void SLPushBack(Seqlist* s, SLDATATYPE x)
{
	SLcheck(s);
	s->data[s->sz] = x;
	s->sz++;
}

// 打印链表数据
void SLprint(Seqlist* s)
{
	int i = 0;
	for (i = 0; i < s->sz; i++)
	{
		printf("%d ", s->data[i]);
	}
	printf("\n");
}

// 尾删
void SLPopBack(Seqlist* s)
{
	if (s->sz == 0)
	{
		printf("SLPopBack:");
		exit(1);
	}
	s->sz--;
}

// 头插
void SLPushFront(Seqlist* s, SLDATATYPE x)
{
	SLcheck(s);
	int i = 0;
	// 数据后移
	for (i = s->sz; i > 0; i--)
	{
		s->data[i] = s->data[i - 1];
	}
	s->data[0] = x;
	s->sz++;
}

// 头删
void SLPopFront(Seqlist* s)
{
	if (s->sz == 0)
	{
		printf("SLPopFront:");
		exit(1);
	}
	for (int i = 0; i < s->sz - 1; i++)
	{
		s->data[i] = s->data[i + 1];
	}
	s->sz--;
}


// 任意位置插入/删除（第pos个元素）
void SLInsert(Seqlist* s, int pos, SLDATATYPE x)
{
	if (pos < 1 || pos >= s->sz + 1)
	{
		printf("SLInsert::pos\n");
		exit(1);
	}
	SLcheck(s);
	for (int i = s->sz; i >= pos; i--)
	{
		s->data[i] = s->data[i - 1];
	}
	s->data[pos - 1] = x;
	s->sz++;
}
void SLErase(Seqlist* s, int pos)
{
	if (pos < 1 || pos > s->sz)
	{
		printf("SLErase::pos\n");
		exit(1);
	}
	for (int i = pos - 1; i < s->sz; i++)
	{
		s->data[i] = s->data[i + 1];
	}
	s->sz--;
}

// 查找，返回找到第一个相同值的下标
int SLFind(Seqlist* s, SLDATATYPE x)
{
	int i = 0;
	for (i = 0; i < s->sz; i++)
	{
		if (s->data[i] == x)
			return i;
	}
	return -1;
}