#pragma once

// 头文件包含
#include<stdio.h>
#include<stdlib.h>

#define  SLDATATYPE int

typedef struct Seqlist
{
	SLDATATYPE* data;   // 数据
	int sz;				// 元素个数
	int capacity;		// 容量大小
}Seqlist;


// 初始化
void SLInit(Seqlist* s);

// 检查容量，空则给5个，满则翻倍
void SLcheck(Seqlist* s);

// 打印链表数据
void SLprint(Seqlist* s);

// 尾插数据
void SLPushBack(Seqlist* s, SLDATATYPE x);

// 尾删
void SLPopBack(Seqlist* s);

// 头插
void SLPushFront(Seqlist* s, SLDATATYPE x);

// 头删
void SLPopFront(Seqlist* s);

// 任意位置插入/删除
void SLInsert(Seqlist* s, int pos, SLDATATYPE x);
void SLErase(Seqlist* s, int pos);

// 查找，返回找到的第一个下标
int SLFind(Seqlist* s, SLDATATYPE x);