#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

struct ListNode {
    int val;
    struct ListNode* next;  
};


// 输出链表中第k个结点, 快慢指针法
struct ListNode* FindKthToTail(struct ListNode* pListHead, int k) {
    struct ListNode* slow, * fast;
    slow = fast = pListHead;
    // fast走k步
    while (k--)
    {
        // fast未走完k步
        if (!fast)
            return NULL;
        fast = fast->next;
    }
    // 同步往后走
    while (fast)
    {
        fast = fast->next;
        slow = slow->next;
    }
    return slow;
}

// 原升序两链表合并为1个升序链表
// 不带头结点
struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2) {
    // 当链表为空时
    if (!list1)
        return list2;
    if (!list2)
        return list1;
    struct ListNode* head, * tail;
    head = tail = NULL;
    while (list1 && list2)
    {
        if (list1->val < list2->val)
        {
            // 为空时
            if (!tail)
                head = tail = list1;
            else
            {
                tail->next = list1;
                tail = tail->next;
            }
            list1 = list1->next;
        }
        else
        {
            if (!tail)
                head = tail = list2;
            else
            {
                tail->next = list2;
                tail = tail->next;
            }
            list2 = list2->next;
        }
    }
    // list有剩余
    if (list1)
        tail->next = list1;
    if (list2)
        tail->next = list2;

    return head;
}
// 带头结点
struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2) {
    struct ListNode* head, * tail;
    head = tail = (struct ListNode*)malloc(sizeof(struct ListNode));
    tail->next = NULL;
    while (list1 && list2)
    {
        if (list1->val < list2->val)
        {

            tail->next = list1;
            tail = tail->next;
            list1 = list1->next;
        }
        else
        {
            tail->next = list2;
            tail = tail->next;
            list2 = list2->next;
        }
    }
    // list有剩余
    if (list1)
        tail->next = list1;
    if (list2)
        tail->next = list2;

    // free申请的空间
    tail = head;
    head = head->next;
    free(tail);
    return head;
}

// 分割链表
struct ListNode* partition(struct ListNode* head, int x) {
    struct ListNode* lesshead, * lesstail, * greaterhead, * greatertail;
    lesshead = lesstail = (struct ListNode*)malloc(sizeof(struct ListNode));
    greaterhead = greatertail = (struct ListNode*)malloc(sizeof(struct ListNode)); \
        lesstail->next = NULL;
    greatertail->next = NULL;
    struct ListNode* cur = head;
    // 将对应的值分别放到两个链表
    while (cur)
    {
        if (cur->val < x)
        {
            lesstail->next = cur;
            lesstail = lesstail->next;
        }
        else
        {
            greatertail->next = cur;
            greatertail = greatertail->next;
        }
        cur = cur->next;
    }
    //连接
    lesstail->next = greaterhead->next;
    //尾部置空
    greatertail->next = NULL;
    head = lesshead->next;
    free(lesshead);
    free(greaterhead);
    return head;
}