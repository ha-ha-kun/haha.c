#define _CRT_SECURE_NO_WARNINGS 1

//数组调用时，其大小须在原定义函数中计算。
//#include<stdio.h>
//int test(int* a[])
//{
//	int sz = sizeof(a) / sizeof(a[0]);
//	printf("test中计算大小为%d\n", sz);
//}
//int main()
//{
//	int a[10] = { 0 };
//	int sz = sizeof(a) / sizeof(a[10]);
//	printf("main中计算大小为%d\n", sz);
//	test(a);
//	return 0;
//}

////冒泡函数的简单实现
//#include<stdio.h>
//void bubble_sort(int a[], int sz)
//{
//	int i = 0, j = 0;
//	int x = 0;
//	for(j = 0; j < sz - 1; j++)
//		for (i = 1; i < sz - j; i++)
//			if (a[i] < a[i - 1])
//			{
//				x = a[i - 1];
//				a[i - 1] = a[i];
//				a[i] = x;
//			}
//}
//
//int main()
//{
//	int a[] = { 8,6,4,2,1,1,6,8,3,2,7 };
//	int sz = sizeof(a) / sizeof(a[0]);
//	int i = 0;
//	printf("数组为：\n");
//	for(i = 0; i < sz; i++)
//		printf("%d ", a[i]);
//	printf("\n");
//	bubble_sort(a, sz);
//	printf("排序后：\n");
//	for (i = 0; i < sz; i++)
//		printf("%d ", a[i]);
//	return 0;
//}

//#include<stdio.h>
void print(int a[], int sz)
{
	int i = 0;
	printf("数组为\n");
	for (i = 0; i < sz; i++)
		printf("%d ", a[i]);
	printf("\n");

}
//void init(int a[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//		a[i] = 0;
//}
//void reverse(int a[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz / 2; i++)
//	{
//		int tmp = a[i];
//		a[i] = a[sz - i - 1];
//		a[sz - i - 1] = tmp;
//	}
//}
//int main()
//{
//	int a[10];
//	int i = 0;
//	int sz = sizeof(a) / sizeof(a[0]);
//	print(a, sz);
//	init(a, sz);
//	printf("初始化后");
//	print(a, sz);
//	for (i = 0; i < sz; i++)
//		a[i] = i;
//	print(a, sz);
//	reverse(a, sz);
//	printf("逆置后");
//	print(a, sz);
//	return 0;
//}

//将两个一样大的数组，替换内容
#include<stdio.h>
void SwapArray(int a[], int b[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		int tmp = a[i];
		a[i] = b[i];
		b[i] = tmp;
	}
}
int main()
{
	int a[] = { 0, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
	int b[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
	int sz = sizeof(a) / sizeof(a[0]);
	printf("a");
	print(a, sz);
	printf("b");
	print(b,sz);
	SwapArray(a, b, sz);
	printf("交换后\n");
	printf("a");
	print(a, sz);
	printf("b");
	print(b, sz);
	return 0;
}