#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<assert.h>

// 给你两个按 非递减顺序 排列的整数数组 nums1 和 nums2，另有两个整数 m 和 n ，分别表示 nums1 和 nums2 中的元素数目。
// 请你 合并 nums2 到 nums1 中，使合并后的数组同样按 非递减顺序 排列。
// 注意：最终，合并后数组不应由函数返回，而是存储在数组 nums1 中。为了应对这种情况，
// nums1 的初始长度为 m + n，其中前 m 个元素表示应合并的元素，后 n 个元素为 0 ，应忽略。nums2 的长度为 n 。

void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n) {
	// 长度为0 或 nums为空指针，断言返回
	assert(nums1 && nums1 && m && n);
	int i = m - 1, j = n - 1;
	int cur = 0;
	//大元素放后面
	while (i >= 0 || j >= 0)
	{
		// nums1中元素已排列完成
		if (i == -1)
		{
			cur = nums2[j--];
		}
		// nums2中元素已排列完成
		else if (j == -1)
		{
			cur = nums1[i--];
		}
		// 大的放后面
		else if (nums1[i] < nums2[j])
		{
			cur = nums2[j--];
		}
		// 相同大小的 nums2的放后面
		else
		{
			cur = nums1[i--];
		}
		// 共有 i+j+2个元素，由于i，j已减，故此时i+j+2便是最后位置
		nums1[i + j + 2] = cur;
	}
}

int main()
{
	int nums1[] = { 1,2,3,0,0,0 };
	int nums2[] = { 2,5,6 };
	int m = 3;
	int n = 3;
	merge(nums1, sizeof(nums1) / sizeof(nums1[0]), m, nums2, sizeof(nums2) / sizeof(nums2[0]), n);
	return 0;
}