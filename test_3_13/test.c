#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//int Max(int x, int y, int z)
//{
//	if (x > y && x > z)
//		return x;
//	else if (y > x && y > z)
//		return y;
//	else
//		return z;
//}
//
//int main()
//{
//	int n1 = 0;
//	int n2 = 0;
//	int n3 = 0;
//	scanf("%d%d%d", &n1, &n2, &n3);//输入
//		int m = Max(n1, n2, n3);//计算
//	printf("%d\n", m);//输出
//	return 0;
//}


#define Elemtype char

#define Max_Tree_size 50  //树中最多结点数
typedef struct {//树的结点定义
	Elemtype data;//数据元素
	int parent;//双亲位置域
}PTNode;//双亲结点命名
typedef struct {//树的类型定义
	PTNode nodes[Max_Tree_size];//双亲表示
	int n;//结点数
}PTree;//双亲表示法

struct CTNode
{
	int child;//孩子结点在数组中的位置
	struct CTNode* next;//下一个孩子
};
typedef struct {
	Elemtype data;
	struct CTNode* firstchild;//第一个孩子
}CTBox;
typedef struct {
	CTBox nodes[Max_Tree_size];
	int n, r;//结点数和根的位置
}CTree;//孩子表示法

//孩子兄弟表示法
struct CSNode
{
	Elemtype data;//数据域
	struct CSNode* firstchild, * nextsibing;//第一个孩子指针以及下一个兄弟指针
}CSNode, * CSTree;//孩子兄弟表示法命名,又称二叉树表示法

