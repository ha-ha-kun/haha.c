#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

// 给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。
// 不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地 修改输入数组。
// 元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。
int removeElement(int* nums, int numsSize, int val) {
	int dst, src;
	dst = src = 0;
	for (src = 0; src < numsSize; src++)
	{
		while (nums[src] == val)
		{
			src++;
		}
		if (src < numsSize)
			nums[dst++] = nums[src];
	}
	return dst;
}

// 删除重复值
int removeDuplicates(int* nums, int numsSize)
{
	int dst, src;
	dst = 0;
	for (src = 1; src < numsSize; src++)
	{
		if (nums[src] != nums[dst])
		{
			nums[++dst] = nums[src];
		}
	}
	return dst + 1;
}

// 合并有序数组
void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n) 
{
	int end1, end2, end;
	end = m + n - 1;
	end1 = m - 1;
	end2 = n - 1;
	while (end1 >= 0 && end2 >= 0)
	{
		if (nums2[end2] > nums1[end1])
		{
			nums1[end--] = nums2[end2--];
		}
		else
		{
			nums1[end--] = nums1[end1--];
		}
	}
	while (end1 < 0 && end2 >= 0)
	{
		nums1[end--] = nums2[end2--];
	}
}
int main()
{
	int n[] = { 1,1,2 };
	int x = removeDuplicates(n, 3);
	return 0;
}