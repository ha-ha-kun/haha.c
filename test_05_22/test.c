#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdbool.h>

struct ListNode
{
    int val;
    struct ListNode* next;
};

// 给你两个单链表的头节点 headA 和 headB ，请你找出并返回两个单链表相交的起始节点。如果两个链表不存在相交节点，返回 null 。
struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB) {
    if (!headA || !headB)
    {
        return NULL;
    }
    int lenA, lenB;
    lenA = 1, lenB = 1;
    struct ListNode* pA, * pB;
    pA = headA, pB = headB;
    // 得链表长度
    while (pA->next)
    {
        lenA++;
        pA = pA->next;
    }
    while (pB->next)
    {
        lenB++;
        pB = pB->next;
    }
    // 长的先走
    struct ListNode* longlist = headA, * shortlist = headB;
    if (lenA < lenB)
    {
        longlist = headB;
        shortlist = headA;
    }
    int differ = abs(lenA - lenB);
    while (differ--)
    {
        longlist = longlist->next;
    }
    // 开始一起走
    while (longlist && shortlist)
    {
        if (longlist == shortlist)
        {
            return longlist;
        }
        longlist = longlist->next;
        shortlist = shortlist->next;
    }
    return NULL;
}

// 判断环
bool hasCycle(struct ListNode* head) {
    struct ListNode* slow, * fast;
    slow = fast = head;
    while (fast && fast->next)
    {
        slow = slow->next;
        fast = fast->next->next;

        if (slow == fast)
        {
            return true;
        }
    }

    return false;
}

// 返回环的起点
struct ListNode* detectCycle(struct ListNode* head) {
    struct ListNode* slow, * fast;
    slow = fast = head;
    while (fast && fast->next)
    {
        slow = slow->next;
        fast = fast->next->next;
        if (slow == fast)
        {
            struct ListNode* meet = slow;
            while (meet != head)
            {
                meet = meet->next;
                head = head->next;
            }

            return meet;
        }
    }
    return NULL;
}