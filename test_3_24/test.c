#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
//一个整型数组 nums 里除两个数字之外，其他数字都出现了两次。请写程序找出这两个只出现一次的数字。要求时间复杂度是O(n)，空间复杂度是O(1)。

//对每一个成员都异或，得到两个不同数异或的结果
int* singleNumbers(int* nums, int numsSize, int* returnSize) {
	//满足限制条件
	if (numsSize < 2 || numsSize > 10000)
	{
		return NULL;
	}
	int sum = 0;
	int i = 0;
	//全员异或得到的结果，是不同的两个数异或的结果
	for (i = 0; i < numsSize; i++)
	{
		sum ^= nums[i];
	}
	//找到两个数中不同的bit位
	int flag = 1;
	while ((flag & sum) == 0)
	{
		flag = flag << 1;
	}
	returnSize[0] = sum;
	returnSize[1] = sum;
	for (i = 0; i < numsSize; i++)
	{
		//找到第一个数
		if (nums[i] & flag)
		{
			returnSize[1] ^= nums[i];
		}
		//找到第二个数
		else
		{
			returnSize[0] ^= nums[i];
		}
	}
	return returnSize;
}

int main()
{
	int a[] = { 4,1,4,6 };
	int b[2] = { 0 };
	singleNumbers(a, sizeof(a) / sizeof(a[0]), b);
	printf("%d %d", b[0], b[1]);
	return 0;
}