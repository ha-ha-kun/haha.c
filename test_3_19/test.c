#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>

// 一个数组中只有两个数字是出现一次，其他所有数字都出现了两次。编写一个函数找出这两个只出现一次的数字。
// 定义辅助数组，利用空间换时间 

//循环
void find_solo(const int* a, int sz)
{
	assert(a);
	//返回数组
	int* flag = (int*)malloc(sizeof(int) * sz);
	int i = 0;
	//ret初始化为1
	for (i = 0; i < sz; i++)
	{
		flag[i] = 1;
	}
	//寻找满足条件的值的位置
	for (i = 0; i < sz - 1; i++)
	{
		int j = 0;
		for (j = i + 1; j < sz; j++)
		{
			if (a[i] == a[j])
			{
				flag[i] = 0;
				flag[j] = 0;
			}
		}
	}
	//对满足条件值的打印
	for (i = 0; i < sz; i++)
	{
		if (flag[i])
			printf("%d ", a[i]);
	}
}

int main()
{
	int a[10] = { 1,2,3,3,1,5,2,6,7,5 };
	find_solo(a, sizeof(a) / sizeof(a[0]));
	return 0;
}