#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<ctype.h>
#include<string.h>

// 模拟实现atoi
// int atoi(const char* string);
// 返回int 
// 1、遇到非数字字符，停止识别，遇到数字字符，识别为整型值，可采用isdigit判别
// 2、存在边界问题，比边界值小(大），返回边界值
// 3、只在第一个位置识别正负符号
// 4、识别完字符串，也就是遇到“\0”时，应该返回
// 5、遇到空白会往后继续识别
//

// 模拟实现atoi
//计算转换字符值
int compute(const char* s)
{
	int ret = 0;
	while (isdigit(*s))
	{
		ret = ret * 10 + (*s++ - '0');
	}
	return ret;
}
int my_atoi(const char* s)
{
	//判空
	assert(s);
	int ret = 0;
	//跳过空白
	while ((*s++) == '\t');
	//判别第一个字符
	if (isdigit(*s))
	{
		ret = compute(s);
	}
	else if (*s == '-')
	{
		ret = -compute(s + 1);
	}
	else if (*s == '+')
	{
		ret = compute(s + 1);
	}
	else
	{
		return 0;
	}
	return ret;
}
int main()
{
	char* c = "   12312\04147";
	int i = 0;
	i = atoi(c);
	//i = my_atoi(c);
	printf("%d\n",i);
	return 0;
}

// strncat
// 1、结束条件：a、src遇到\0时，结束拷贝； b、拷贝完n个字符
//

// 模拟实现strncpy
char* my_strncpy(char* dest, const char* src, int n)
{
	//判空
	assert(dest && src);
	char* ret = dest;
	int i = 0;
	for (i = 0; i < n; i++)
	{
		if (*src == '\0')
			break;
		*dest++ = *src++;
	}
	return ret;
}

// 模拟实现strncat
char* my_strncat(char* dest, const char* src, int n)
{
	//判空
	assert(dest && src);
	char* ret = dest;
	int i = 0;
	//寻找插入点
	while (*dest != '\0')
	{
		dest++;
	}
	for (i = 0; i < n; i++)
	{
		if (*src == '\0')
			break;
		*dest++ = *src++;
	}
	return ret;
}
//int main()
//{
//	char a[] = "hello\0dadfa";
//	char b[20] = { "world " };
//	printf("%s", my_strncat(b, a, 7));
//	return 0;
//}