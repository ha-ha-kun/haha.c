#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<assert.h>

//模拟实现memmove
void* my_memmove(void* dest, const void* src, size_t n)
{
	//断言确保不为空指针
	assert(dest && src);
	void* ret = dest;
	//从前往后复制
	if (dest < src)
	{
		while (n--)
		{
			*(char*)dest = *(char*)src;
			dest = (char*)dest + 1;
			src = (char*)src + 1;
		}
	}
	//从后往前复制
	else
	{
		while (n--)
		{
			*((char*)dest + n) = *((char*)src + n);
		}
	}
	return ret;
}

void* my_memcpy(void* dest, const void* src, size_t n)
{
	assert(dest && src);
	void* ret = dest;
	//从前往后复制
	while (n--)
	{
		*(char*)dest = *(char*)src;
		dest = (char*)dest + 1;
		src = (char*)src + 1;
	}
	return ret;
}

int my_memcmp(const void* e1, const void* e2, size_t n)
{
	assert(e1 && e2);
	while (n--)
	{
		if (*(char*)e1 != *(char*)e2)
		{
			return *(char*)e1 - *(char*)e2;
		}
		e1 = (char*)e1 + 1;
		e2 = (char*)e2 + 1;
	}
	return 0;
}

void* my_memset(void* dest, int c, size_t n)
{
	assert(dest);
	void* ret = dest;
	while (n--)
	{
		*(char*)dest = (char)c;
		dest = (char*)dest + 1;
	}
	return ret;
}
int main()
{
	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
	int arr2[20] = { 1,2,3,4,0x1122334405 };
	my_memset(arr2, 0, 20);
	//printf("%s", my_memcmp(arr1, arr2, 17));
	return 0;
}