#define _CRT_SECURE_NO_WARNINGS 1

////输出整数的每一位
//#include<stdio.h>
//int main()
//{
//	int x = 0;
//	scanf("%d", &x);
//	printf("百位是%d, 十位是%d, 个位是%d", x / 100, (x % 100) / 10, x % 10);
//	return 0;
//}

////递归求n的阶乘
//#include<stdio.h>
//int factor(int n)
//{
//	if (1 == n)
//		return 1;
//	else
//		return n * factor(n - 1);
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	printf("%d\n",factor(n));
//	return 0;
//}

////非递归求n的阶乘
//#include<stdio.h>
//int main()
//{
//	int n = 0, i = 0, pro = 1;
//	scanf("%d", &n);
//	for (i = 1; i <= n; i++)
//		pro *= i;
//	printf("%d", pro);
//	return 0;
//}

////非递归实现strlen
//#include<stdio.h>
//int strlen1(char a[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//		if (a[i] == '\0')
//			return i;
//	return 0;
//}
//int main()
//{
//	int sz = 20;
//	char a[20] = { 0 };
//	scanf("%s", a);
//	printf("%d", strlen1(a, sz));
//	return 0;
//

////递归实现strlen
//#include<stdio.h>
//int strlen2(char a[], int sz)
//{
//	if (a[sz] == '\0')
//		return 1 + strlen2(a, sz - 1);
//	else
//		return 0;
//}
//int main()
//{
//	int sz = 20;
//	char a[20] = { 0 };
//	scanf("%s", a);
//	printf("%d", sz - strlen2(a, sz - 1));
//	return 0;
//}

////字符串逆序实现
//#include<stdio.h>
//void reverse_string(char* string)
//{
//	int i = 0, j = 0;
//	char a = 0;
//	for (i = 0; i < 20 && string[i] != '\0'; i++);
//	i--;
//	for (j = 0; j < i / 2 + 1; j++)
//	{
//		a = string[j];
//		string[j] = string[i - j];
//		string[i - j] = a;
//	}
//}
//int main()
//{
//	char a[20] = { 0 };
//	scanf("%s", a);
//	reverse_string(a);
//	printf("%s", a);
//	return 0;
//}

////递归实现计算正整数的每位之和
//int DigitSum(int a)
//{
//	if (a / 10 == 0)
//		return a % 10;
//	else
//		return DigitSum(a / 10) + a % 10;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	printf("%d",DigitSum(a));
//
//	return 0;
//}

////递归实现n的k次方
//#include<stdio.h>
//int ToThePower(int n, int k)
//{
//	if (0 == k)
//		return 1;
//	else
//		return n * ToThePower(n, k - 1);
//}
//int main()
//{
//	int n = 0, k = 0;
//	scanf("%d %d", &n, &k);
//	printf("%d", ToThePower(n, k));
//}

//计算斐波拉契数
#include<stdio.h>
int Fabonacci(int i)
{
	if (1 == i || 2 == i)
		return 1;
	else
		return Fabonacci(i - 1) + Fabonacci(i - 2);
}
int main()
{
	int i = 0;
	scanf("%d", &i);
	printf("%d", Fabonacci(i));
	return 0;
}