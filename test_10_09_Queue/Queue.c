#define _CRT_SECURE_NO_WARNINGS 1

#include"Queue.h"

void QueueInit(Queue* q)
{
	assert(q);
	q->front = q->rear = NULL;
}

void QueuePush(Queue* q, QDataType data)
{
	assert(q);
	QNode* tmp = (QNode*)malloc(sizeof(QNode));
	if (!tmp)
	{
		perror("malloc failed");
		exit(-1);
	}
	tmp->data = data;
	tmp->next = NULL;
	// 没有元素
	if (q->front == NULL)
	{
		q->front = q->rear = tmp;
	}
	else
	{
		q->rear->next = tmp;
		q->rear = tmp;
	}
}

void QueuePop(Queue* q)
{
	assert(q);
	// 只有一个元素
	if (q->front == q->rear)
	{
		free(q->front);
		q->front = q->rear = NULL;
	}
	else
	{
		QNode* tmp = q->front;
		q->front = tmp->next;
		free(tmp);
	}
}

QDataType QueueFront(Queue* q)
{
	assert(q);

	return q->front->data;
}

QDataType QueueBack(Queue* q)
{
	assert(q);

	return q->rear->data;
}

int QueueSize(Queue* q)
{
	assert(q);
	
	QNode* cur = q->front;
	int size = 0;
	while (cur != q->rear)
	{
		cur = cur->next;
		size++;
	}
	return size;
}

int QueueEmpty(Queue* q)
{
	assert(q);

	return q->front == NULL;
}

void QueueDestroy(Queue* q)
{
	assert(q);

	while (q->front != NULL)
	{
		QueuePop(q);
	}
}