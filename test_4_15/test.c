#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<stdlib.h>
// 数组形式的整数加法
int* addToArrayForm(int* num, int numSize, int k, int* returnSize) {
	// 逐位相加
	int flag = 0, tmp = k, i = 0, itmp = 0;
	*returnSize = 0;
	// 求申请空间的大小
	for (i = numSize - 1; i >= 0; i--)
	{
		(*returnSize)++;
		itmp = num[i] + tmp % 10 + flag;
		tmp /= 10;
		flag = itmp / 10;
	}
	// k值比数组值大，此时还有值
	if (tmp > 0)
	{
		while (flag > 0 || tmp > 0)
		{
			itmp = tmp % 10 + flag;
			(*returnSize)++;
			tmp /= 10;
			flag = itmp / 10;
		}
	}
	else
	{
		if (flag > 0)
		{
			(*returnSize)++;

		}
	}
	// 申请空间并赋值
	int* ptmp = (int*)malloc((*returnSize) * sizeof(int));
	if (ptmp == NULL)
		return NULL;
	tmp = k,flag = 0;
	int j = 0;
	for (j = (*returnSize) - 1, i = numSize - 1; i >= 0 && j >= 0; i--, j--)
	{
		itmp = num[i] + tmp % 10 + flag;
		tmp /= 10;
		flag = itmp / 10;
		ptmp[j] = itmp % 10;
	}
	// 若k值大
	if (tmp > 0)
	{
		while (flag > 0 || tmp > 0)
		{
			itmp = tmp % 10 + flag;
			ptmp[j--] = itmp % 10;
			tmp /= 10;
			flag = itmp / 10;
		}
	}
	else
	{
		if (flag > 0)
		{
			ptmp[0] = flag;
		}
	}
	return ptmp;
}

int main()
{
	int n[] = { 0 };
	int k = 23;
	int numsize = 0;
	int* a = addToArrayForm(n, 1, k, &numsize);
 	return 0;
}