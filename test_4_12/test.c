#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
// 给定一个整数数组 nums，将数组中的元素向右轮转 k 个位置，其中 k 是非负数。
// 每次转1个，转k次
void rot1(int* nums, int numsSize)
{
	int dst = numsSize - 1;
	int src;
	int tmp = nums[dst];
	for (src = dst - 1; src >= 0; src--)
	{
		nums[dst--] = nums[src];
	}
	nums[0] = tmp;
}
void rotate1(int* nums, int numsSize, int k) {
	for (int i = 0; i < k; i++)
	{
		rot1(nums, numsSize);
	}
}

// 基于反转数组
void rollback(int* nums, int start, int end)
{
	int tmp = 0;
	while (start < end)
	{
		tmp = nums[start];
		nums[start++] = nums[end];
		nums[end--] = tmp;
	}
}
void rotate(int* nums, int numsSize, int k) {
	k %= numsSize;
	rollback(nums, 0, numsSize - k - 1);
	rollback(nums, numsSize - k, numsSize - 1);
	rollback(nums, 0, numsSize - 1);
}

void testrotate()
{
	int nums[] = { 1,2,3,4,5,6,7 };
	rotate(nums, 7, 3);
}

int* addToArrayForm(int* num, int numSize, int k, int* returnSize) {
	// int sum = 0;
	int index = 1;
	int i = 0;
	for (i = numSize - 1; i >= 0; i--)
	{
		// 会数据溢出
		//sum *= 10;
		//sum += num[i];
		k += (num[i] * index);
		index *= 10;
	}
	//k += sum;
	*returnSize = 0;
	for (i = 1; k / i != 0; i *= 10)
	{
		(*returnSize)++;
	}
	int* tmp = malloc((*returnSize) * sizeof(int));
	for (i = *returnSize - 1; i >= 0; i--)
	{
		*(tmp + i) = k % 10;
		k /= 10;
	}
	return tmp;
}

void test2()
{
	int num[] = { 1,2,0,0 };
	int k = 34;
	int n = 0;
	addToArrayForm(num, 4, k, &n);
}


int main()
{
	test2();
	return 0;
}