#define _CRT_SECURE_NO_WARNINGS 1

#include"game.h"
//选择菜单
void menu()
{
	printf("*********************\n");
	printf("*** 1、开始游戏 *****\n");
	printf("*** 0、退出游戏 *****\n");
	printf("*********************\n");
}

//初始化棋盘
void Initboard(char  board[ROW][COL], int row, int col)

{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
			board[i][j] = ' ';//初始化为' '
	}
}
//void Initboard(char  (* board)[COL], int row, int col)
//
//{
//	int i = 0;
//	for (i = 0; i < row; i++)
//	{
//		int j = 0;
//		for (j = 0; j < col; j++)
//			(board[i])[j] = ' ';
//	}
//}

//打印棋盘
void Displayboard(char board[ROW][COL], int row, int col)
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			printf(" %c ", board[i][j]);
			if (j < col -1)
				printf("|");//棋盘竖向分割线
		}
		if(i < row -1)
			printf("\n-----------\n");//棋盘横向分割线
	}
	printf("\n");
}

//玩家下棋
void player_move(char board[ROW][COL], int row, int col)
{
	int x = 0, y = 0;
	printf("玩家下\n");
	while (1)
	{
		printf("请输入要下的坐标：");
		scanf("%d %d", &x, &y);
		if (x > 0 && x <= row && y > 0 && y <= col)
		{
			if (board[x - 1][y - 1] == ' ')
			{
				board[x - 1][y - 1] = '*';
				break;
			}
			else
				printf("坐标已被占用，请重新输入\n");
		}
		else
			printf("坐标非法，请重新输入\n");
	}
	
}

//电脑下棋
void computer_move(char board[ROW][COL], int row, int col)
{
	int x = 0, y = 0;
	
	printf("电脑下\n");
	while (1)
	{
		x = rand() % row;
		y = rand() % col;
		if (board[x][y] == ' ')
		{
			board[x][y] = '#';
			break;
		}
	}
}

//判断输赢
//玩家赢返回‘*’，输返回‘#’，平局返回‘Q’，继续返回‘C’
static int isfull(char board[ROW][COL], int row, int col)
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			if (board[i][j] == ' ')
				return 0;//棋盘没满
		}
	}
	return 1;//棋盘满了
}
char Iswin(char board[ROW][COL], int row, int col)
{
	int i = 0;
	//判断列
	for (i = 0; i < row; i++)
	{
		if (board[i][0] == board[i][1] && board[i][0] == board[i][2] && board[i][0] != ' ')
			return board[i][0];
	}
	//判断行
	for (i = 0; i < row; i++)
	{
		if (board[0][i] == board[1][i] && board[0][i] == board[2][i] && board[0][i] != ' ')
			return board[0][i];
	}
	//判断对角线
	if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != ' ' || board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] != ' ')
		return board[1][1];
	if (isfull(board, row, col))
		return 'Q';
	else
		return 'C';
}

//游戏函数
void game()
{
	char ret = 0;//用以判断输赢
	//棋盘
	char board[ROW][COL] = { 0 };
	//初始化棋盘 
	Initboard(board, ROW, COL);
	//打印棋盘
	Displayboard(board, ROW, COL);
	while (1)
	{
		
		//玩家下棋
		player_move(board, ROW, COL);
		Displayboard(board, ROW, COL);
		//判断输赢
		ret = Iswin(board, ROW, COL);
		if (ret != 'C')
			break;
		//电脑下棋
		computer_move(board, ROW, COL);
		Displayboard(board, ROW, COL);
		//判断输赢
		ret = Iswin(board, ROW, COL);
		if (ret != 'C')
			break;
	}
	if (ret == '*')
		printf("你赢了\n");
	else if (ret == '#')
		printf("你输了\n");
	else
		printf("平局\n");
}
