#define _CRT_SECURE_NO_WARNINGS 1

#include<stdlib.h>
#include<stdio.h>
#include<stdbool.h>

struct ListNode 
{
    int val;
    struct ListNode* next;
};
#include<stdio.h>


// 移出链表中等于val的结点
// 1、逐个移除
// 2、将不等于val的结点依次尾插， 可定义一个空的头结点
struct ListNode* removeElements(struct ListNode* head, int val)
{
    struct ListNode* cur = head;
    struct ListNode* pre = NULL;
    // 跳过开始等于val的点
    while (cur)
    {
        if (cur->val == val)
        {
            // 头删
            if (cur == head)
            {
                head = cur->next;
                free(cur);
                cur = head;
            }
            else
            {
                // 删除
                pre->next = cur->next;
                free(cur);
                cur = pre->next;
            }
        }
        else
        {
            pre = cur;
            cur = cur->next;
        }
    }
    return head;
}

// 给你单链表的头结点 head ，请你找出并返回链表的中间结点。
struct ListNode* middleNode(struct ListNode* head) {
    struct ListNode* pNode[100] = { 0 };
    int count = 0;
    for (count = 0; head != NULL; count++)
    {
        pNode[count] = head;
        head = head->next;
    }
    return pNode[count / 2];
}


// 输入一个链表，输出该链表中倒数第k个结点
struct ListNode* FindKthToTail(struct ListNode* pListHead, int k) {
    // write code here
    struct ListNode* pNode[100] = { 0 };
    int count = 0;
    while (pListHead)
    {
        pNode[count++] = pListHead;
        pListHead = pListHead->next;
    }
    if (k <= count)
        return pNode[count - k];
    else
        return pListHead;
}

// 将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。
struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2) {
    struct ListNode* head = (struct ListNode*)malloc(sizeof(struct ListNode));
    head->next = NULL;
    struct ListNode* cur = head;
    while (list1 && list2)
    {
        if (list1->val < list2->val)
        {
            cur->next = list1;
            list1 = list1->next;
        }
        else
        {
            cur->next = list2;
            list2 = list2->next;
        }
        cur = cur->next;
    }
    while (list1)
    {
        cur->next = list1;
        list1 = list1->next;
    }
}

// 现有一链表的头指针 ListNode* pHead，给一定值x，编写一段代码将所有小于x的结点排在其余结点之前，且不能改变原来的数据顺序，返回重新排列后的链表的头指针。
struct ListNode* partition(struct ListNode* head, int x) {
    struct ListNode* more = (struct ListNode*)malloc(sizeof(struct ListNode));
    struct ListNode* less = (struct ListNode*)malloc(sizeof(struct ListNode));
    struct ListNode* tmp1 = more, * tmp2 = less;
    while (head)
    {
        if (head->val < x)
        {
            less->next = head;
            less = less->next;
        }
        else
        {
            more->next = head;
            more = more->next;
        }
        head = head->next;
    }
    more->next = NULL; // 结尾置空
    less->next = tmp1->next; // 连接
    head = tmp2->next; // 返回头指针
    free(tmp1);
    free(tmp2);
    return head;
}

// 回文链表
typedef struct ListNode node;
bool isPalindrome(struct ListNode* head) {
    node* slow, * fast, * prev;
    slow = fast = head;
    prev = NULL;
    while (fast)
    {
        slow = slow->next;
        fast = fast->next ? fast->next->next : fast->next;
    }
    node* tmp = NULL;
    while (slow)
    {
        // 头插
        tmp = slow->next;
        slow->next = prev;
        prev = slow;
        slow = tmp;
    }
    while (prev && head)
    {
        if (head->val != prev->val)
            return false;
        prev = prev->next;
        head = head->next;
    }
    return true;

}
int main()
{
   
}